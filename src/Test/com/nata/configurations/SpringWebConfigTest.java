package com.nata.configurations;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring6.view.ThymeleafViewResolver;

public class SpringWebConfigTest {

    private SpringWebConfig springWebConfigUnderTest;

    @Before
    public void setUp() throws Exception {
        springWebConfigUnderTest = new SpringWebConfig();
    }

    @Test
    public void testTemplateResolver() {
        // Setup
        // Run the test
        final SpringResourceTemplateResolver result = springWebConfigUnderTest.templateResolver();

        // Verify the results
    }

    @Test
    public void testTemplateEngine() {
        // Setup
        // Run the test
        final SpringTemplateEngine result = springWebConfigUnderTest.templateEngine();

        // Verify the results
    }

    @Test
    public void testViewResolver() {
        // Setup
        // Run the test
        final ThymeleafViewResolver result = springWebConfigUnderTest.viewResolver();

        // Verify the results
    }
}