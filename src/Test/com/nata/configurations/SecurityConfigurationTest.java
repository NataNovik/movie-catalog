package com.nata.configurations;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Map;

import static org.junit.Assert.assertThrows;

public class SecurityConfigurationTest {

    private SecurityConfiguration securityConfigurationUnderTest;

    @Before
    public void setUp() throws Exception {
        securityConfigurationUnderTest = new SecurityConfiguration();
    }

    @Test
    public void testSecurityFilterChain() throws Exception {
        // Setup
        final HttpSecurity http = new HttpSecurity(null, new AuthenticationManagerBuilder(null),
                Map.ofEntries(Map.entry(String.class, "value")));

        // Run the test
        final SecurityFilterChain result = securityConfigurationUnderTest.securityFilterChain(http);

        // Verify the results
    }

    @Test
    public void testSecurityFilterChain_ThrowsException() {
        // Setup
        final HttpSecurity http = new HttpSecurity(null, new AuthenticationManagerBuilder(null),
                Map.ofEntries(Map.entry(String.class, "value")));

        // Run the test
        assertThrows(Exception.class, () -> securityConfigurationUnderTest.securityFilterChain(http));
    }

    @Test
    public void testUserDetailsService() {
        // Setup
        // Run the test
        final UserDetailsService result = securityConfigurationUnderTest.userDetailsService();

        // Verify the results
    }

    @Test
    public void testPasswordEncoder() {
        // Setup
        // Run the test
        final BCryptPasswordEncoder result = securityConfigurationUnderTest.passwordEncoder();

        // Verify the results
    }

    @Test
    public void testAuthenticationProvider() {
        // Setup
        // Run the test
        final DaoAuthenticationProvider result = securityConfigurationUnderTest.authenticationProvider();

        // Verify the results
    }
}