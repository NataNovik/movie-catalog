package com.nata.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CollectionDtoTest {

    @Mock
    private Set<MovieDto> mockMovies;

    private CollectionDto collectionDtoUnderTest;

    @Before
    public void setUp() throws Exception {
        collectionDtoUnderTest = new CollectionDto(0, "name", mockMovies);
    }

    @Test
    public void testEquals() {
        assertFalse(collectionDtoUnderTest.equals("o"));
    }

    @Test
    public void testHashCode() {
        assertEquals(367668528, collectionDtoUnderTest.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("CollectionDto(id = 0, name = name, movies = mockMovies)", collectionDtoUnderTest.toString());
    }
}