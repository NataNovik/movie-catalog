package com.nata.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CompanyDtoTest {

    @Mock
    private Set<MovieDto> mockMovies;

    private CompanyDto companyDtoUnderTest;

    @Before
    public void setUp() throws Exception {
        companyDtoUnderTest = new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                "originCountryCode", 0, "parentCompanyName", mockMovies);
    }

    @Test
    public void testEquals() {
        assertFalse(companyDtoUnderTest.equals("o"));
    }

    @Test
    public void testHashCode() {
        assertEquals(-356815101, companyDtoUnderTest.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("CompanyDto(id = 0, homepage = homepage, description = description, headquerters = headquerters, logoPath = logoPath, name = name, originCountryCode = originCountryCode, parentCompanyId = 0, parentCompanyName = parentCompanyName, movies = mockMovies)", companyDtoUnderTest.toString());
    }
}