package com.nata.dto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CrewCreditDtoTest {

    @Mock
    private MovieDto mockFilm;
    @Mock
    private DepartmentDto mockDepartment;
    @Mock
    private PersonDto mockPerson;
    @Mock
    private JobDto mockJob;

    private CrewCreditDto crewCreditDtoUnderTest;

    @Before
    public void setUp() throws Exception {
        crewCreditDtoUnderTest = new CrewCreditDto("creditId", mockFilm, mockDepartment, mockPerson, mockJob);
    }

    @Test
    public void testEquals() {
        assertFalse(crewCreditDtoUnderTest.equals("o"));
    }

    @Test
    public void testHashCode() {
        assertEquals(457162368, crewCreditDtoUnderTest.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("CrewCreditDto(creditId = creditId, film = mockFilm, department = mockDepartment, person = mockPerson, job = mockJob)", crewCreditDtoUnderTest.toString());
    }
}