package com.nata.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RoleTest {

    private Role roleUnderTest;

    @Before
    public void setUp() throws Exception {
        roleUnderTest = new Role(0, "name");
    }

    @Test
    public void testToString() {
        assertEquals("Role{id=0, name='name'}", roleUnderTest.toString());
    }
}