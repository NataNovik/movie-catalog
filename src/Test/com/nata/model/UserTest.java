package com.nata.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {

    private User userUnderTest;

    @Before
    public void setUp() throws Exception {
        userUnderTest = new User(0, "email", "fullName", "password", false);
    }

    @Test
    public void testToString() {
        assertEquals("User{id=0, email='email', full_name='fullName', password='password', enabled=false, roles=[]}", userUnderTest.toString());
    }
}