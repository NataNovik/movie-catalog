package com.nata.controller;

import com.nata.model.User;
import com.nata.repository.UserRepository;
import com.nata.services.CountryService;
import com.nata.services.MovieService;
import com.nata.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @Mock
    private UserService mockUserService;
    @Mock
    private CountryService mockCountryService;
    @Mock
    private MovieService mockMovieService;
    @Mock
    private UserRepository mockUserRepository;

    private UserController userControllerUnderTest;

    @Before
    public void setUp() throws Exception {
        userControllerUnderTest = new UserController(mockUserService, mockCountryService, mockMovieService,
                mockUserRepository);
    }

    @Test
    public void testCurrentPage() {
        // Setup
        // Run the test
        final Integer result = userControllerUnderTest.currentPage();

        // Verify the results
        assertEquals(Integer.valueOf(0), result);
    }

    @Test
    public void testTotalPages() {
        // Setup
        // Run the test
        final Integer result = userControllerUnderTest.totalPages();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testTotalItems() {
        // Setup
        // Run the test
        final Integer result = userControllerUnderTest.totalItems();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testPageSize() {
        // Setup
        // Run the test
        final Integer result = userControllerUnderTest.pageSize();

        // Verify the results
        assertEquals(Integer.valueOf(10), result);
    }

    @Test
    public void testRegisterUser1() {
        // Setup
        // Run the test
        final String result = userControllerUnderTest.registerUser();

        // Verify the results
        assertEquals("user/user_add_form", result);
    }

    @Test
    public void testRegisterUser2() {
        // Setup
        final ResponseEntity<?> expectedResult = new ResponseEntity<>(null, HttpStatusCode.valueOf(0));

        // Run the test
        final ResponseEntity<?> result = userControllerUnderTest.registerUser("signUpRequest");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testEditProfile() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure UserService.getUserByUserName(...).
        final User user = new User(0, "email", "fullName", "password", false);
        when(mockUserService.getUserByUserName("userName")).thenReturn(user);

        // Run the test
        final String result = userControllerUnderTest.editProfile(model);

        // Verify the results
        assertEquals("user/user_edit_form", result);
    }

    @Test
    public void testEditUser() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure UserService.findById(...).
        final User user = new User(0, "email", "fullName", "password", false);
        when(mockUserService.findById(0)).thenReturn(user);

        // Run the test
        final String result = userControllerUnderTest.editUser(0, model);

        // Verify the results
        assertEquals("user/user_edit_form", result);
    }

    @Test
    public void testUpdateUser() {
        // Setup
        final User user = new User(0, "email", "fullName", "password", false);
        final BindingResult bindingResult = null;

        // Run the test
        final String result = userControllerUnderTest.updateUser(user, bindingResult);

        // Verify the results
        assertEquals("redirect:/users", result);
        verify(mockUserService).update(any(User.class));
    }

    @Test
    public void testCreateUser1() {
        // Setup
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = userControllerUnderTest.createUser(model);

        // Verify the results
        assertEquals("user/user_add_form", result);
    }

    @Test
    public void testCreateUser2() {
        // Setup
        final User user = new User(0, "email", "fullName", "password", false);
        final BindingResult result1 = null;
        final Model model = new ConcurrentModel();

        // Configure UserRepository.findAll(...).
        final List<User> users = List.of(new User(0, "email", "fullName", "password", false));
        when(mockUserRepository.findAll()).thenReturn(users);

        // Run the test
        final String result = userControllerUnderTest.createUser(user, result1, model);

        // Verify the results
        assertEquals("redirect:/users", result);
        verify(mockUserService).update(any(User.class));
    }

    @Test
    public void testCreateUser2_UserRepositoryReturnsNoItems() {
        // Setup
        final User user = new User(0, "email", "fullName", "password", false);
        final BindingResult result1 = null;
        final Model model = new ConcurrentModel();
        when(mockUserRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final String result = userControllerUnderTest.createUser(user, result1, model);

        // Verify the results
        assertEquals("redirect:/users", result);
        verify(mockUserService).update(any(User.class));
    }

    @Test
    public void testDeleteUser() {
        // Setup
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = userControllerUnderTest.deleteUser(0, model, Optional.of(0));

        // Verify the results
        assertEquals("redirect:/users", result);
        verify(mockUserService).deleteById(0);
    }

    @Test
    public void testSelectAll() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure UserService.findPaginated(...).
        final Page<User> users = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserService.findPaginated(0, 0)).thenReturn(users);

        // Configure UserService.findPaginatedWithFilterByFullNameIgnoreCase(...).
        final Page<User> users1 = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName")).thenReturn(users1);

        // Run the test
        final String result = userControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("user/user_list", result);
    }

    @Test
    public void testSelectAll_UserServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockUserService.findPaginated(0, 0)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final String result = userControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("user/user_list", result);
    }

    @Test
    public void testSelectAll_UserServiceFindPaginatedWithFilterByFullNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockUserService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName"))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final String result = userControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("user/user_list", result);
    }

    @Test
    public void testSelectAllData() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure UserService.findPaginated(...).
        final Page<User> users = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserService.findPaginated(0, 0)).thenReturn(users);

        // Configure UserService.findPaginatedWithFilterByFullNameIgnoreCase(...).
        final Page<User> users1 = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName")).thenReturn(users1);

        // Run the test
        final ModelAndView result = userControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testSelectAllData_UserServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockUserService.findPaginated(0, 0)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final ModelAndView result = userControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testSelectAllData_UserServiceFindPaginatedWithFilterByFullNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockUserService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName"))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final ModelAndView result = userControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }
}