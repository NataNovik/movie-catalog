package com.nata.controller;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class LoginControllerTest {

    private LoginController loginControllerUnderTest;

    @Before
    public void setUp() {
        loginControllerUnderTest = new LoginController();
    }

    @Test
    public void testLoginView() {
        // Setup
        // Run the test
        final String result = loginControllerUnderTest.loginView();

        // Verify the results
        assertEquals("common/login_page", result);
    }

    @Test
    public void testLogout() {
        // Setup
        // Run the test
        final String result = loginControllerUnderTest.logout(Optional.of("value"));

        // Verify the results
        assertEquals("common/logout_page", result);
    }

    @Test
    public void testLogin() {
        // Setup
        // Run the test
        final String result = loginControllerUnderTest.login("login");

        // Verify the results
        assertEquals("common/home", result);
    }

    @Test
    public void testDenyAccess() {
        // Setup
        // Run the test
        final String result = loginControllerUnderTest.DenyAccess("login");

        // Verify the results
        assertEquals("common/no_access", result);
    }
}