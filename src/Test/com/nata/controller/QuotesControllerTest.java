package com.nata.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuotesControllerTest {

    private QuotesController quotesControllerUnderTest;

    @Before
    public void setUp() {
        quotesControllerUnderTest = new QuotesController();
    }

    @Test
    public void testSelectAll() {
        // Setup
        // Run the test
        final String result = quotesControllerUnderTest.selectAll();

        // Verify the results
        assertEquals("quotes/quotes_list", result);
    }
}