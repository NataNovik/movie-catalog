package com.nata.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import static org.junit.Assert.assertEquals;

public class ErrControllerTest {

    private ErrController errControllerUnderTest;

    @Before
    public void setUp() {
        errControllerUnderTest = new ErrController();
    }

    @Test
    public void testHandleError() {
        // Setup
        final HttpServletRequest request = null;
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = errControllerUnderTest.handleError(request, model);

        // Verify the results
        assertEquals("common/error", result);
    }
}