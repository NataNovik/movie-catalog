package com.nata.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AwardsControllerTest {

    private AwardsController awardsControllerUnderTest;

    @Before
    public void setUp() {
        awardsControllerUnderTest = new AwardsController();
    }

    @Test
    public void testSelectAll() {
        // Setup
        // Run the test
        final String result = awardsControllerUnderTest.selectAll();

        // Verify the results
        assertEquals("awards/awards", result);
    }
}