package com.nata.controller;

import com.nata.services.CountryService;
import com.nata.services.MovieService;
import com.nata.services.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class HomeControllerTest {

    private HomeController homeControllerUnderTest;

    @Before
    public void setUp() throws Exception {
        homeControllerUnderTest = new HomeController();
        homeControllerUnderTest.movieService = mock(MovieService.class);
        homeControllerUnderTest.personService = mock(PersonService.class);
        homeControllerUnderTest.countryService = mock(CountryService.class);
    }

    @Test
    public void testSelectAll() {
        // Setup
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = homeControllerUnderTest.selectAll(model, Optional.of("value"));

        // Verify the results
        assertEquals("common/home", result);
    }
}