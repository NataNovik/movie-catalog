package com.nata.controller;

import com.nata.dto.*;
import com.nata.mappers.CountryMapper;
import com.nata.mappers.DepartmentMapper;
import com.nata.mappers.GenderMapper;
import com.nata.mappers.PersonMapper;
import com.nata.model.*;
import com.nata.services.CountryService;
import com.nata.services.DepartmentService;
import com.nata.services.GenderService;
import com.nata.services.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {

    @Mock
    private PersonMapper mockPersonMapper;
    @Mock
    private CountryMapper mockCountryMapper;
    @Mock
    private DepartmentMapper mockDepartmentMapper;
    @Mock
    private GenderMapper mockGenderMapper;
    @Mock
    private PersonService mockPersonService;
    @Mock
    private CountryService mockCountryService;
    @Mock
    private DepartmentService mockDepartmentService;
    @Mock
    private GenderService mockGenderService;

    private PersonController personControllerUnderTest;

    @Before
    public void setUp() {
        personControllerUnderTest = new PersonController(mockPersonMapper, mockCountryMapper, mockDepartmentMapper,
                mockGenderMapper, mockPersonService, mockCountryService, mockDepartmentService, mockGenderService);
    }

    @Test
    public void testCurrentPage() {
        // Setup
        // Run the test
        final Integer result = personControllerUnderTest.currentPage();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testTotalPages() {
        // Setup
        // Run the test
        final Integer result = personControllerUnderTest.totalPages();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testTotalItems() {
        // Setup
        // Run the test
        final Integer result = personControllerUnderTest.totalItems();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testPageSize() {
        // Setup
        // Run the test
        final Integer result = personControllerUnderTest.pageSize();

        // Verify the results
        assertEquals(Integer.valueOf(10), result);
    }

    @Test
    public void testEditPerson() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findById(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);
        when(mockPersonService.findById(0)).thenReturn(person);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        // Configure GenderService.findAll(...).
        final Gender gender1 = new Gender();
        gender1.setId((byte) 0b0);
        gender1.setGenderName("genderName");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        gender1.setPeople(Set.of(person1));
        final Collection<Gender> genders = List.of(gender1);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));

        // Configure DepartmentService.findAll(...).
        final Department department1 = new Department();
        department1.setId((byte) 0b0);
        department1.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department1.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department1);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person2 = new Person();
        person2.setId(0);
        person2.setName("name");
        country.setPersons(Set.of(person2));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.editPerson(0, model);

        // Verify the results
        assertEquals("person/person_edit_form", result);
    }

    @Test
    public void testEditPerson_GenderServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findById(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);
        when(mockPersonService.findById(0)).thenReturn(person);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        when(mockGenderService.findAll()).thenReturn(Collections.emptyList());

        // Configure DepartmentService.findAll(...).
        final Department department1 = new Department();
        department1.setId((byte) 0b0);
        department1.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department1.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department1);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        country.setPersons(Set.of(person1));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.editPerson(0, model);

        // Verify the results
        assertEquals("person/person_edit_form", result);
    }

    @Test
    public void testEditPerson_DepartmentServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findById(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);
        when(mockPersonService.findById(0)).thenReturn(person);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        // Configure GenderService.findAll(...).
        final Gender gender1 = new Gender();
        gender1.setId((byte) 0b0);
        gender1.setGenderName("genderName");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        gender1.setPeople(Set.of(person1));
        final Collection<Gender> genders = List.of(gender1);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));
        when(mockDepartmentService.findAll()).thenReturn(Collections.emptyList());

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person2 = new Person();
        person2.setId(0);
        person2.setName("name");
        country.setPersons(Set.of(person2));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.editPerson(0, model);

        // Verify the results
        assertEquals("person/person_edit_form", result);
    }

    @Test
    public void testEditPerson_CountryServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findById(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);
        when(mockPersonService.findById(0)).thenReturn(person);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        // Configure GenderService.findAll(...).
        final Gender gender1 = new Gender();
        gender1.setId((byte) 0b0);
        gender1.setGenderName("genderName");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        gender1.setPeople(Set.of(person1));
        final Collection<Gender> genders = List.of(gender1);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));

        // Configure DepartmentService.findAll(...).
        final Department department1 = new Department();
        department1.setId((byte) 0b0);
        department1.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department1.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department1);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        when(mockCountryService.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final String result = personControllerUnderTest.editPerson(0, model);

        // Verify the results
        assertEquals("person/person_edit_form", result);
    }

    @Test
    public void testUpdatePerson() {
        // Setup
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);

        // Configure GenderService.findById(...).
        final Gender gender1 = new Gender();
        gender1.setId((byte) 0b0);
        gender1.setGenderName("genderName");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        gender1.setPeople(Set.of(person1));
        when(mockGenderService.findById(0)).thenReturn(gender1);

        // Configure DepartmentService.findById(...).
        final Department department1 = new Department();
        department1.setId((byte) 0b0);
        department1.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department1.setCrewCredits(Set.of(crewCredit));
        when(mockDepartmentService.findById(0)).thenReturn(department1);

        // Configure CountryService.findById(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person2 = new Person();
        person2.setId(0);
        person2.setName("name");
        country.setPersons(Set.of(person2));
        when(mockCountryService.findById(0)).thenReturn(country);

        // Run the test
        final String result = personControllerUnderTest.updatePerson(person, Optional.of(0), Optional.of(0),
                Optional.of(0));

        // Verify the results
        assertEquals("redirect:/persons", result);
        verify(mockPersonService).update(any(Person.class));
    }

    @Test
    public void testCreatePerson1() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure GenderService.findAll(...).
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));
        final Collection<Gender> genders = List.of(gender);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));

        // Configure DepartmentService.findAll(...).
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        country.setPersons(Set.of(person1));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.createPerson(model);

        // Verify the results
        assertEquals("person/person_add_form", result);
    }

    @Test
    public void testCreatePerson1_GenderServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockGenderService.findAll()).thenReturn(Collections.emptyList());

        // Configure DepartmentService.findAll(...).
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        country.setPersons(Set.of(person));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.createPerson(model);

        // Verify the results
        assertEquals("person/person_add_form", result);
    }

    @Test
    public void testCreatePerson1_DepartmentServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure GenderService.findAll(...).
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));
        final Collection<Gender> genders = List.of(gender);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));
        when(mockDepartmentService.findAll()).thenReturn(Collections.emptyList());

        // Configure CountryService.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        country.setPersons(Set.of(person1));
        final Collection<Country> countries = List.of(country);
        when(mockCountryService.findAll()).thenReturn(countries);

        when(mockCountryMapper.toDto(any(Country.class))).thenReturn(new CountryDto("code", "country"));

        // Run the test
        final String result = personControllerUnderTest.createPerson(model);

        // Verify the results
        assertEquals("person/person_add_form", result);
    }

    @Test
    public void testCreatePerson1_CountryServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure GenderService.findAll(...).
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));
        final Collection<Gender> genders = List.of(gender);
        when(mockGenderService.findAll()).thenReturn(genders);

        when(mockGenderMapper.toDto(any(Gender.class))).thenReturn(new GenderDto((byte) 0b0, "genderName"));

        // Configure DepartmentService.findAll(...).
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));
        final Collection<Department> departments = List.of(department);
        when(mockDepartmentService.findAll()).thenReturn(departments);

        // Configure DepartmentMapper.toDto(...).
        final DepartmentDto departmentDto = new DepartmentDto((byte) 0b0, "department");
        when(mockDepartmentMapper.toDto(any(Department.class))).thenReturn(departmentDto);

        when(mockCountryService.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final String result = personControllerUnderTest.createPerson(model);

        // Verify the results
        assertEquals("person/person_add_form", result);
    }

    @Test
    public void testCreatePerson2() {
        // Setup
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        person.setDepartment(department);
        final Gender gender = new Gender();
        person.setGender(gender);
        final Country countryCode = new Country();
        person.setCountryCode(countryCode);

        final BindingResult result1 = null;
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = personControllerUnderTest.createPerson(person, result1, model);

        // Verify the results
        assertEquals("create_person", result);
        verify(mockPersonService).update(any(Person.class));
    }

    @Test
    public void testDeletePerson() {
        // Setup
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = personControllerUnderTest.deletePerson(0, model, Optional.of(0));

        // Verify the results
        assertEquals("redirect:/persons", result);
        verify(mockPersonService).deleteById(0);
    }

    @Test
    public void testSelectAll() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginated(0, 0, PersonGridDto.class)).thenReturn(new PageImpl<>(List.of()));
        when(mockPersonService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName",
                PersonGridDto.class)).thenReturn(new PageImpl<>(List.of()));

        // Run the test
        final String result = personControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("person/person_list", result);
    }

    @Test
    public void testSelectAll_PersonServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginated(0, 0, PersonGridDto.class))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final String result = personControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("person/person_list", result);
    }

    @Test
    public void testSelectAll_PersonServiceFindPaginatedWithFilterByFullNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName",
                PersonGridDto.class)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final String result = personControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("person/person_list", result);
    }

    @Test
    public void testSelectAllData() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginated(0, 0, PersonGridDto.class)).thenReturn(new PageImpl<>(List.of()));
        when(mockPersonService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName",
                PersonGridDto.class)).thenReturn(new PageImpl<>(List.of()));

        // Run the test
        final ModelAndView result = personControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testSelectAllData_PersonServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginated(0, 0, PersonGridDto.class))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final ModelAndView result = personControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testSelectAllData_PersonServiceFindPaginatedWithFilterByFullNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginatedWithFilterByFullNameIgnoreCase(0, 0, "fullName",
                PersonGridDto.class)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final ModelAndView result = personControllerUnderTest.selectAllData(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }
}