package com.nata.controller;

import com.nata.dto.*;
import com.nata.mappers.LanguageMapper;
import com.nata.mappers.MovieMapper;
import com.nata.mappers.PersonMapper;
import com.nata.model.*;
import com.nata.repository.LanguageRepository;
import com.nata.repository.MovieRepository;
import com.nata.services.CountryService;
import com.nata.services.MovieService;
import com.nata.services.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieControllerTest {

    @Mock
    private MovieService mockMovieService;
    @Mock
    private PersonService mockPersonService;
    @Mock
    private CountryService mockCountryService;
    @Mock
    private MovieRepository mockMovieRepository;
    @Mock
    private MovieMapper mockMovieMapper;
    @Mock
    private PersonMapper mockPersonMapper;
    @Mock
    private LanguageRepository mockLanguageRepository;
    @Mock
    private LanguageMapper mockLanguageMapper;

    private MovieController movieControllerUnderTest;

    @Before
    public void setUp() throws Exception {
        movieControllerUnderTest = new MovieController(mockMovieService, mockPersonService, mockCountryService,
                mockMovieRepository, mockMovieMapper, mockPersonMapper, mockLanguageRepository, mockLanguageMapper);
    }

    @Test
    public void testGetCurrentPage() {
        // Setup
        // Run the test
        final Integer result = movieControllerUnderTest.getCurrentPage();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testGetTotalPages() {
        // Setup
        // Run the test
        final Integer result = movieControllerUnderTest.getTotalPages();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testGetTotalItems() {
        // Setup
        // Run the test
        final Integer result = movieControllerUnderTest.getTotalItems();

        // Verify the results
        assertEquals(Integer.valueOf(1), result);
    }

    @Test
    public void testGetPageSize() {
        // Setup
        // Run the test
        final Integer result = movieControllerUnderTest.getPageSize();

        // Verify the results
        assertEquals(Integer.valueOf(10), result);
    }

    @Test
    public void testGetSearch() {
        // Setup
        // Run the test
        final String result = movieControllerUnderTest.getSearch();

        // Verify the results
        assertEquals("", result);
    }

    @Test
    public void testEditMovie() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure MovieService.findById(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        when(mockMovieService.findById(0)).thenReturn(movie);

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Configure PersonService.findAll(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Collection<Person> people = List.of(person);
        when(mockPersonService.findAll()).thenReturn(people);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        // Configure LanguageRepository.findAll(...).
        final Language language = new Language();
        language.setCode("code");
        language.setName("name");
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        language.setMoviesWithOriginalLanguage(Set.of(movie1));
        final List<Language> languages = List.of(language);
        when(mockLanguageRepository.findAll()).thenReturn(languages);

        when(mockLanguageMapper.toDto(any(Language.class))).thenReturn(new LanguageDto("code", "name"));

        // Run the test
        final String result = movieControllerUnderTest.editMovie(0, model);

        // Verify the results
        assertEquals("movie/movie_edit_form", result);
    }

    @Test
    public void testEditMovie_PersonServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure MovieService.findById(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        when(mockMovieService.findById(0)).thenReturn(movie);

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        when(mockPersonService.findAll()).thenReturn(Collections.emptyList());

        // Configure LanguageRepository.findAll(...).
        final Language language = new Language();
        language.setCode("code");
        language.setName("name");
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        language.setMoviesWithOriginalLanguage(Set.of(movie1));
        final List<Language> languages = List.of(language);
        when(mockLanguageRepository.findAll()).thenReturn(languages);

        when(mockLanguageMapper.toDto(any(Language.class))).thenReturn(new LanguageDto("code", "name"));

        // Run the test
        final String result = movieControllerUnderTest.editMovie(0, model);

        // Verify the results
        assertEquals("movie/movie_edit_form", result);
    }

    @Test
    public void testEditMovie_LanguageRepositoryReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure MovieService.findById(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        when(mockMovieService.findById(0)).thenReturn(movie);

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Configure PersonService.findAll(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Collection<Person> people = List.of(person);
        when(mockPersonService.findAll()).thenReturn(people);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        when(mockLanguageRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final String result = movieControllerUnderTest.editMovie(0, model);

        // Verify the results
        assertEquals("movie/movie_edit_form", result);
    }

    @Test
    public void testUpdateMovie() {
        // Setup
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movie = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));

        // Configure MovieMapper.toEntity(...).
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        movie1.setBackdropPath("backdropPath");
        movie1.setBudget(0);
        movie1.setHomepage("homepage");
        when(mockMovieMapper.toEntity(any(MovieDto.class))).thenReturn(movie1);

        // Run the test
        final String result = movieControllerUnderTest.updateMovie(movie, new int[]{0});

        // Verify the results
        assertEquals("redirect:/movies", result);
        verify(mockMovieService).update(any(Movie.class));
    }

    @Test
    public void testCreateMovie1() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findPaginated(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Page<Person> people = new PageImpl<>(List.of(person));
        when(mockPersonService.findPaginated(1, 20)).thenReturn(people);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        // Configure LanguageRepository.findAll(...).
        final Language language = new Language();
        language.setCode("code");
        language.setName("name");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        language.setMoviesWithOriginalLanguage(Set.of(movie));
        final List<Language> languages = List.of(language);
        when(mockLanguageRepository.findAll()).thenReturn(languages);

        when(mockLanguageMapper.toDto(any(Language.class))).thenReturn(new LanguageDto("code", "name"));

        // Run the test
        final String result = movieControllerUnderTest.createMovie(model);

        // Verify the results
        assertEquals("movie/movie_add_form", result);
    }

    @Test
    public void testCreateMovie1_PersonServiceReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockPersonService.findPaginated(1, 20)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Configure LanguageRepository.findAll(...).
        final Language language = new Language();
        language.setCode("code");
        language.setName("name");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        language.setMoviesWithOriginalLanguage(Set.of(movie));
        final List<Language> languages = List.of(language);
        when(mockLanguageRepository.findAll()).thenReturn(languages);

        when(mockLanguageMapper.toDto(any(Language.class))).thenReturn(new LanguageDto("code", "name"));

        // Run the test
        final String result = movieControllerUnderTest.createMovie(model);

        // Verify the results
        assertEquals("movie/movie_add_form", result);
    }

    @Test
    public void testCreateMovie1_LanguageRepositoryReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure PersonService.findPaginated(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Page<Person> people = new PageImpl<>(List.of(person));
        when(mockPersonService.findPaginated(1, 20)).thenReturn(people);

        // Configure PersonMapper.toDto(...).
        final PersonDto personDto = new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"),
                LocalDate.of(2020, 1, 1), "alsoKnownAs", LocalDate.of(2020, 1, 1),
                new GenderDto((byte) 0b0, "genderName"), "biography", "profilePath", new BigDecimal("0.00"),
                "placeOfBirth", "imdbId", "homepage", new CountryDto("code", "country"), "additionalInfo");
        when(mockPersonMapper.toDto(any(Person.class))).thenReturn(personDto);

        when(mockLanguageRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final String result = movieControllerUnderTest.createMovie(model);

        // Verify the results
        assertEquals("movie/movie_add_form", result);
    }

    @Test
    public void testCreateMovie2() {
        // Setup
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movie = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        final BindingResult result1 = null;
        final Model model = new ConcurrentModel();

        // Configure MovieMapper.toEntity(...).
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        movie1.setBackdropPath("backdropPath");
        movie1.setBudget(0);
        movie1.setHomepage("homepage");
        when(mockMovieMapper.toEntity(any(MovieDto.class))).thenReturn(movie1);

        // Run the test
        final String result = movieControllerUnderTest.createMovie(movie, result1, model);

        // Verify the results
        assertEquals("movie/movie_add_form", result);
        verify(mockMovieService).update(any(Movie.class));
    }

    @Test
    public void testDeleteMovie() {
        // Setup
        final Model model = new ConcurrentModel();

        // Run the test
        final String result = movieControllerUnderTest.deleteMovie(0, model, Optional.of(0));

        // Verify the results
        assertEquals("redirect:/movies", result);
        verify(mockMovieService).deleteById(0);
    }

    @Test
    public void testSelectAll() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure MovieService.findPaginated(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        final Page<Movie> movies = new PageImpl<>(List.of(movie));
        when(mockMovieService.findPaginated(0, 0)).thenReturn(movies);

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Configure MovieService.findPaginatedWithFilterByNameIgnoreCase(...).
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        movie1.setBackdropPath("backdropPath");
        movie1.setBudget(0);
        movie1.setHomepage("homepage");
        final Page<Movie> movies1 = new PageImpl<>(List.of(movie1));
        when(mockMovieService.findPaginatedWithFilterByNameIgnoreCase(0, 0, "fullName")).thenReturn(movies1);

        // Run the test
        final String result = movieControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("movie/movie_list", result);
    }

    @Test
    public void testSelectAll_MovieServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockMovieService.findPaginated(0, 0)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Run the test
        final String result = movieControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("movie/movie_list", result);
    }

    @Test
    public void testSelectAll_MovieServiceFindPaginatedWithFilterByNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockMovieService.findPaginatedWithFilterByNameIgnoreCase(0, 0, "fullName"))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Run the test
        final String result = movieControllerUnderTest.selectAll(model, Optional.of(0), Optional.of(0),
                Optional.of("value"));

        // Verify the results
        assertEquals("movie/movie_list", result);
    }

    @Test
    public void testRefreshDataForGrid() {
        // Setup
        final Model model = new ConcurrentModel();

        // Configure MovieService.findPaginated(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        final Page<Movie> movies = new PageImpl<>(List.of(movie));
        when(mockMovieService.findPaginated(0, 0)).thenReturn(movies);

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Configure MovieService.findPaginatedWithFilterByNameIgnoreCase(...).
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        movie1.setBackdropPath("backdropPath");
        movie1.setBudget(0);
        movie1.setHomepage("homepage");
        final Page<Movie> movies1 = new PageImpl<>(List.of(movie1));
        when(mockMovieService.findPaginatedWithFilterByNameIgnoreCase(0, 0, "fullName")).thenReturn(movies1);

        // Run the test
        final ModelAndView result = movieControllerUnderTest.refreshDataForGrid(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testRefreshDataForGrid_MovieServiceFindPaginatedReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockMovieService.findPaginated(0, 0)).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Run the test
        final ModelAndView result = movieControllerUnderTest.refreshDataForGrid(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }

    @Test
    public void testRefreshDataForGrid_MovieServiceFindPaginatedWithFilterByNameIgnoreCaseReturnsNoItems() {
        // Setup
        final Model model = new ConcurrentModel();
        when(mockMovieService.findPaginatedWithFilterByNameIgnoreCase(0, 0, "fullName"))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Configure MovieMapper.toDto(...).
        final MovieReview movieReview = new MovieReview();
        movieReview.setId((short) 0);
        final Movie film = new Movie();
        film.setId(0);
        film.setTitle("title");
        film.setBackdropPath("backdropPath");
        movieReview.setFilm(film);
        final MovieDto movieDto = new MovieDto(0, "title", "backdropPath", 0, "homepage", "imdbId",
                new LanguageDto("code", "name"), "originalTitle", "overview", new BigDecimal("0.00"), "posterPath",
                LocalDate.of(2020, 1, 1), 0L, "runtime", new BigDecimal("0.00"), (short) 0, "status", "tagline",
                "trailerLink", Set.of(new CrewCreditDto("creditId", null, new DepartmentDto((byte) 0b0, "department"),
                new PersonDto(0, "name", new DepartmentDto((byte) 0b0, "department"), LocalDate.of(2020, 1, 1),
                        "alsoKnownAs", LocalDate.of(2020, 1, 1), new GenderDto((byte) 0b0, "genderName"), "biography",
                        "profilePath", new BigDecimal("0.00"), "placeOfBirth", "imdbId", "homepage",
                        new CountryDto("code", "country"), "additionalInfo"), new JobDto((short) 0, "job"))),
                Set.of(new CompanyDto(0, "homepage", "description", "headquerters", "logoPath", "name",
                        "originCountryCode", 0, "parentCompanyName", Set.of())),
                Set.of(new GenreDto((short) 0, "genre")), Set.of("value"), Set.of(), Set.of(), Set.of(movieReview),
                Set.of(), Set.of(), Set.of(new LanguageDto("code", "name")));
        when(mockMovieMapper.toDto(any(Movie.class))).thenReturn(movieDto);

        // Run the test
        final ModelAndView result = movieControllerUnderTest.refreshDataForGrid(Optional.of(0), Optional.of(0),
                Optional.of("value"), model);

        // Verify the results
    }
}