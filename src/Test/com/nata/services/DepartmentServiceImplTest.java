package com.nata.services;

import com.nata.model.CrewCredit;
import com.nata.model.Department;
import com.nata.model.Movie;
import com.nata.repository.DepartmentRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DepartmentServiceImplTest {

    private DepartmentServiceImpl departmentServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        departmentServiceImplUnderTest = new DepartmentServiceImpl();
        departmentServiceImplUnderTest.departmentRepository = mock(DepartmentRepository.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure DepartmentRepository.findAll(...).
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));
        final List<Department> departments = List.of(department);
        when(departmentServiceImplUnderTest.departmentRepository.findAll()).thenReturn(departments);

        // Run the test
        final Collection<Department> result = departmentServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_DepartmentRepositoryReturnsNoItems() {
        // Setup
        when(departmentServiceImplUnderTest.departmentRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Department> result = departmentServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure DepartmentRepository.findById(...).
        final Department department1 = new Department();
        department1.setId((byte) 0b0);
        department1.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department1.setCrewCredits(Set.of(crewCredit));
        final Optional<Department> department = Optional.of(department1);
        when(departmentServiceImplUnderTest.departmentRepository.findById(0)).thenReturn(department);

        // Run the test
        final Department result = departmentServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_DepartmentRepositoryReturnsAbsent() {
        // Setup
        when(departmentServiceImplUnderTest.departmentRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Department result = departmentServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));

        // Run the test
        departmentServiceImplUnderTest.insert(department);

        // Verify the results
        verify(departmentServiceImplUnderTest.departmentRepository).save(any(Department.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        final CrewCredit crewCredit = new CrewCredit();
        crewCredit.setCreditId("creditId");
        final Movie film = new Movie();
        crewCredit.setFilm(film);
        department.setCrewCredits(Set.of(crewCredit));

        // Run the test
        departmentServiceImplUnderTest.update(department);

        // Verify the results
        verify(departmentServiceImplUnderTest.departmentRepository).save(any(Department.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        departmentServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(departmentServiceImplUnderTest.departmentRepository).deleteById(0);
    }
}