package com.nata.services;

import com.nata.model.User;
import com.nata.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private PasswordEncoder mockPasswordEncoder;

    @InjectMocks
    private UserServiceImpl userServiceImplUnderTest;

    @Test
    public void testFindAll() {
        // Setup
        // Configure UserRepository.findAll(...).
        final List<User> users = List.of(new User(0, "email", "fullName", "password", false));
        when(mockUserRepository.findAll()).thenReturn(users);

        // Run the test
        final Collection<User> result = userServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_UserRepositoryReturnsNoItems() {
        // Setup
        when(mockUserRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<User> result = userServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure UserRepository.findById(...).
        final Optional<User> user = Optional.of(new User(0, "email", "fullName", "password", false));
        when(mockUserRepository.findById(0)).thenReturn(user);

        // Run the test
        final User result = userServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_UserRepositoryReturnsAbsent() {
        // Setup
        when(mockUserRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final User result = userServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final User entity = new User(0, "email", "fullName", "password", false);
        when(mockPasswordEncoder.encode("password")).thenReturn("password");

        // Run the test
        userServiceImplUnderTest.insert(entity);

        // Verify the results
        verify(mockUserRepository).save(any(User.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final User entity = new User(0, "email", "fullName", "password", false);
        when(mockPasswordEncoder.encode("password")).thenReturn("password");

        // Run the test
        userServiceImplUnderTest.update(entity);

        // Verify the results
        verify(mockUserRepository).save(any(User.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        userServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(mockUserRepository).deleteById(0);
    }

    @Test
    public void testGetUserByUserName() {
        // Setup
        // Configure UserRepository.getUserByUserName(...).
        final User user = new User(0, "email", "fullName", "password", false);
        when(mockUserRepository.getUserByUserName("userName")).thenReturn(user);

        // Run the test
        final User result = userServiceImplUnderTest.getUserByUserName("userName");

        // Verify the results
    }

    @Test
    public void testFindPaginated() {
        // Setup
        // Configure UserRepository.findAll(...).
        final Page<User> users = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserRepository.findAll(any(Pageable.class))).thenReturn(users);

        // Run the test
        final Page<User> result = userServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginated_UserRepositoryReturnsNoItems() {
        // Setup
        when(mockUserRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<User> result = userServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase() {
        // Setup
        // Configure UserRepository.findByFullNameIgnoreCaseContaining(...).
        final Page<User> users = new PageImpl<>(List.of(new User(0, "email", "fullName", "password", false)));
        when(mockUserRepository.findByFullNameIgnoreCaseContaining(any(Pageable.class), eq("fullNamePart")))
                .thenReturn(users);

        // Run the test
        final Page<User> result = userServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullNamePart");

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase_UserRepositoryReturnsNoItems() {
        // Setup
        when(mockUserRepository.findByFullNameIgnoreCaseContaining(any(Pageable.class), eq("fullNamePart")))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<User> result = userServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullNamePart");

        // Verify the results
    }
}