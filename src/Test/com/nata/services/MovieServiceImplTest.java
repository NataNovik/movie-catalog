package com.nata.services;

import com.nata.model.Movie;
import com.nata.repository.MovieRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class MovieServiceImplTest {

    private MovieServiceImpl movieServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        movieServiceImplUnderTest = new MovieServiceImpl();
        movieServiceImplUnderTest.movieRepository = mock(MovieRepository.class);
        movieServiceImplUnderTest.personService = mock(PersonService.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure MovieRepository.findAll(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        final List<Movie> movies = List.of(movie);
        when(movieServiceImplUnderTest.movieRepository.findAll()).thenReturn(movies);

        // Run the test
        final Collection<Movie> result = movieServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_MovieRepositoryReturnsNoItems() {
        // Setup
        when(movieServiceImplUnderTest.movieRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Movie> result = movieServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure MovieRepository.findById(...).
        final Movie movie1 = new Movie();
        movie1.setId(0);
        movie1.setTitle("title");
        movie1.setBackdropPath("backdropPath");
        movie1.setBudget(0);
        movie1.setHomepage("homepage");
        final Optional<Movie> movie = Optional.of(movie1);
        when(movieServiceImplUnderTest.movieRepository.findById(0)).thenReturn(movie);

        // Run the test
        final Movie result = movieServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_MovieRepositoryReturnsAbsent() {
        // Setup
        when(movieServiceImplUnderTest.movieRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Movie result = movieServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Movie entity = new Movie();
        entity.setId(0);
        entity.setTitle("title");
        entity.setBackdropPath("backdropPath");
        entity.setBudget(0);
        entity.setHomepage("homepage");

        // Run the test
        movieServiceImplUnderTest.insert(entity);

        // Verify the results
        verify(movieServiceImplUnderTest.movieRepository).save(any(Movie.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");

        // Run the test
        movieServiceImplUnderTest.update(movie);

        // Verify the results
        verify(movieServiceImplUnderTest.movieRepository).save(any(Movie.class));
    }

    @Test
    public void testFindPaginated() {
        // Setup
        // Configure MovieRepository.findAll(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        final Page<Movie> movies = new PageImpl<>(List.of(movie));
        when(movieServiceImplUnderTest.movieRepository.findAll(any(Pageable.class))).thenReturn(movies);

        // Run the test
        final Page<Movie> result = movieServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginated_MovieRepositoryReturnsNoItems() {
        // Setup
        when(movieServiceImplUnderTest.movieRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<Movie> result = movieServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByNameIgnoreCase() {
        // Setup
        // Configure MovieRepository.findByTitleIgnoreCaseContaining(...).
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        movie.setBackdropPath("backdropPath");
        movie.setBudget(0);
        movie.setHomepage("homepage");
        final Page<Movie> movies = new PageImpl<>(List.of(movie));
        when(movieServiceImplUnderTest.movieRepository.findByTitleIgnoreCaseContaining(any(Pageable.class),
                eq("namePart"))).thenReturn(movies);

        // Run the test
        final Page<Movie> result = movieServiceImplUnderTest.findPaginatedWithFilterByNameIgnoreCase(0, 0, "namePart");

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByNameIgnoreCase_MovieRepositoryReturnsNoItems() {
        // Setup
        when(movieServiceImplUnderTest.movieRepository.findByTitleIgnoreCaseContaining(any(Pageable.class),
                eq("namePart"))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<Movie> result = movieServiceImplUnderTest.findPaginatedWithFilterByNameIgnoreCase(1, 0, "namePart");

        // Verify the results
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        movieServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(movieServiceImplUnderTest.movieRepository).deleteById(0);
    }
}