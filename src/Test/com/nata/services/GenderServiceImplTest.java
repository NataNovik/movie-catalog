package com.nata.services;

import com.nata.model.Gender;
import com.nata.model.Person;
import com.nata.repository.GenderRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GenderServiceImplTest {

    private GenderServiceImpl genderServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        genderServiceImplUnderTest = new GenderServiceImpl();
        genderServiceImplUnderTest.genderRepository = mock(GenderRepository.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure GenderRepository.findAll(...).
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));
        final List<Gender> genders = List.of(gender);
        when(genderServiceImplUnderTest.genderRepository.findAll()).thenReturn(genders);

        // Run the test
        final Collection<Gender> result = genderServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_GenderRepositoryReturnsNoItems() {
        // Setup
        when(genderServiceImplUnderTest.genderRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Gender> result = genderServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure GenderRepository.findById(...).
        final Gender gender1 = new Gender();
        gender1.setId((byte) 0b0);
        gender1.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender1.setPeople(Set.of(person));
        final Optional<Gender> gender = Optional.of(gender1);
        when(genderServiceImplUnderTest.genderRepository.findById(0)).thenReturn(gender);

        // Run the test
        final Gender result = genderServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_GenderRepositoryReturnsAbsent() {
        // Setup
        when(genderServiceImplUnderTest.genderRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Gender result = genderServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));

        // Run the test
        genderServiceImplUnderTest.insert(gender);

        // Verify the results
        verify(genderServiceImplUnderTest.genderRepository).save(any(Gender.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Gender gender = new Gender();
        gender.setId((byte) 0b0);
        gender.setGenderName("genderName");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        gender.setPeople(Set.of(person));

        // Run the test
        genderServiceImplUnderTest.update(gender);

        // Verify the results
        verify(genderServiceImplUnderTest.genderRepository).save(any(Gender.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        genderServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(genderServiceImplUnderTest.genderRepository).deleteById(0);
    }
}