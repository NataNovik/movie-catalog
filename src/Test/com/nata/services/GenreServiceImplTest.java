package com.nata.services;

import com.nata.model.Genre;
import com.nata.model.Movie;
import com.nata.repository.GenreRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GenreServiceImplTest {

    private GenreServiceImpl genreServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        genreServiceImplUnderTest = new GenreServiceImpl();
        genreServiceImplUnderTest.GenreRepository = mock(GenreRepository.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure GenreRepository.findAll(...).
        final Genre genre = new Genre();
        genre.setId((short) 0);
        genre.setGenre("genre");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        genre.setMovies(Set.of(movie));
        final List<Genre> genres = List.of(genre);
        when(genreServiceImplUnderTest.GenreRepository.findAll()).thenReturn(genres);

        // Run the test
        final Collection<Genre> result = genreServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_GenreRepositoryReturnsNoItems() {
        // Setup
        when(genreServiceImplUnderTest.GenreRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Genre> result = genreServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure GenreRepository.findById(...).
        final Genre genre1 = new Genre();
        genre1.setId((short) 0);
        genre1.setGenre("genre");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        genre1.setMovies(Set.of(movie));
        final Optional<Genre> genre = Optional.of(genre1);
        when(genreServiceImplUnderTest.GenreRepository.findById(0)).thenReturn(genre);

        // Run the test
        final Genre result = genreServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_GenreRepositoryReturnsAbsent() {
        // Setup
        when(genreServiceImplUnderTest.GenreRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Genre result = genreServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Genre Genre = new Genre();
        Genre.setId((short) 0);
        Genre.setGenre("genre");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        Genre.setMovies(Set.of(movie));

        // Run the test
        genreServiceImplUnderTest.insert(Genre);

        // Verify the results
        verify(genreServiceImplUnderTest.GenreRepository).save(any(com.nata.model.Genre.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Genre Genre = new Genre();
        Genre.setId((short) 0);
        Genre.setGenre("genre");
        final Movie movie = new Movie();
        movie.setId(0);
        movie.setTitle("title");
        Genre.setMovies(Set.of(movie));

        // Run the test
        genreServiceImplUnderTest.update(Genre);

        // Verify the results
        verify(genreServiceImplUnderTest.GenreRepository).save(any(com.nata.model.Genre.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        genreServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(genreServiceImplUnderTest.GenreRepository).deleteById(0);
    }
}