package com.nata.services;

import com.nata.model.Department;
import com.nata.model.Person;
import com.nata.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class PersonServiceImplTest {

    private PersonServiceImpl personServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        personServiceImplUnderTest = new PersonServiceImpl();
        personServiceImplUnderTest.personRepository = mock(PersonRepository.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure PersonRepository.findAll(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final List<Person> people = List.of(person);
        when(personServiceImplUnderTest.personRepository.findAll()).thenReturn(people);

        // Run the test
        final Collection<Person> result = personServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_PersonRepositoryReturnsNoItems() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Person> result = personServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure PersonRepository.findById(...).
        final Person person1 = new Person();
        person1.setId(0);
        person1.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person1.setDepartment(department);
        final Optional<Person> person = Optional.of(person1);
        when(personServiceImplUnderTest.personRepository.findById(0)).thenReturn(person);

        // Run the test
        final Person result = personServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_PersonRepositoryReturnsAbsent() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Person result = personServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Person entity = new Person();
        entity.setId(0);
        entity.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        entity.setDepartment(department);

        // Run the test
        personServiceImplUnderTest.insert(entity);

        // Verify the results
        verify(personServiceImplUnderTest.personRepository).save(any(Person.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);

        // Run the test
        personServiceImplUnderTest.update(person);

        // Verify the results
        verify(personServiceImplUnderTest.personRepository).save(any(Person.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        personServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(personServiceImplUnderTest.personRepository).deleteById(0);
    }

    @Test
    public void testFindPaginated1() {
        // Setup
        // Configure PersonRepository.findAll(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Page<Person> people = new PageImpl<>(List.of(person));
        when(personServiceImplUnderTest.personRepository.findAll(any(Pageable.class))).thenReturn(people);

        // Run the test
        final Page<Person> result = personServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginated1_PersonRepositoryReturnsNoItems() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<Person> result = personServiceImplUnderTest.findPaginated(0, 0);

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase1() {
        // Setup
        // Configure PersonRepository.findByNameIgnoreCaseContaining(...).
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        final Department department = new Department();
        department.setId((byte) 0b0);
        department.setDepartment("department");
        person.setDepartment(department);
        final Page<Person> people = new PageImpl<>(List.of(person));
        when(personServiceImplUnderTest.personRepository.findByNameIgnoreCaseContaining(any(Pageable.class),
                eq("fullNamePart"))).thenReturn(people);

        // Run the test
        final Page<Person> result = personServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullNamePart");

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase1_PersonRepositoryReturnsNoItems() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findByNameIgnoreCaseContaining(any(Pageable.class),
                eq("fullNamePart"))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<Person> result = personServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullNamePart");

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase2() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findByNameIgnoreCaseContaining(any(Pageable.class),
                eq("fullName"), eq(String.class))).thenReturn(new PageImpl<>(List.of("value")));

        // Run the test
        final Page<String> result = personServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullName", String.class);

        // Verify the results
    }

    @Test
    public void testFindPaginatedWithFilterByFullNameIgnoreCase2_PersonRepositoryReturnsNoItems() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findByNameIgnoreCaseContaining(any(Pageable.class),
                eq("fullName"), eq(String.class))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<String> result = personServiceImplUnderTest.findPaginatedWithFilterByFullNameIgnoreCase(0, 0,
                "fullName", String.class);

        // Verify the results
    }

    @Test
    public void testFindPaginated2() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findBy(any(Pageable.class), eq(String.class)))
                .thenReturn(new PageImpl<>(List.of("value")));

        // Run the test
        final Page<String> result = personServiceImplUnderTest.findPaginated(1, 1, String.class);

        // Verify the results
    }

    @Test
    public void testFindPaginated2_PersonRepositoryReturnsNoItems() {
        // Setup
        when(personServiceImplUnderTest.personRepository.findBy(any(Pageable.class), eq(String.class)))
                .thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        final Page<String> result = personServiceImplUnderTest.findPaginated(2, 2, String.class);

        // Verify the results
    }
}