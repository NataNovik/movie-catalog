package com.nata.services;

import com.nata.model.Country;
import com.nata.model.Person;
import com.nata.repository.CountryRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CountryServiceImplTest {

    private CountryServiceImpl countryServiceImplUnderTest;

    @Before
    public void setUp() throws Exception {
        countryServiceImplUnderTest = new CountryServiceImpl();
        countryServiceImplUnderTest.countryRepository = mock(CountryRepository.class);
    }

    @Test
    public void testFindAll() {
        // Setup
        // Configure CountryRepository.findAll(...).
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        country.setPersons(Set.of(person));
        final List<Country> countries = List.of(country);
        when(countryServiceImplUnderTest.countryRepository.findAll()).thenReturn(countries);

        // Run the test
        final Collection<Country> result = countryServiceImplUnderTest.findAll();

        // Verify the results
    }

    @Test
    public void testFindAll_CountryRepositoryReturnsNoItems() {
        // Setup
        when(countryServiceImplUnderTest.countryRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final Collection<Country> result = countryServiceImplUnderTest.findAll();

        // Verify the results
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void testFindById() {
        // Setup
        // Configure CountryRepository.findById(...).
        final Country country1 = new Country();
        country1.setCode("code");
        country1.setCountry("country");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        country1.setPersons(Set.of(person));
        final Optional<Country> country = Optional.of(country1);
        when(countryServiceImplUnderTest.countryRepository.findById(0)).thenReturn(country);

        // Run the test
        final Country result = countryServiceImplUnderTest.findById(0);

        // Verify the results
    }

    @Test
    public void testFindById_CountryRepositoryReturnsAbsent() {
        // Setup
        when(countryServiceImplUnderTest.countryRepository.findById(0)).thenReturn(Optional.empty());

        // Run the test
        final Country result = countryServiceImplUnderTest.findById(0);

        // Verify the results
        assertNull(result);
    }

    @Test
    public void testInsert() {
        // Setup
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        country.setPersons(Set.of(person));

        // Run the test
        countryServiceImplUnderTest.insert(country);

        // Verify the results
        verify(countryServiceImplUnderTest.countryRepository).save(any(Country.class));
    }

    @Test
    public void testUpdate() {
        // Setup
        final Country country = new Country();
        country.setCode("code");
        country.setCountry("country");
        final Person person = new Person();
        person.setId(0);
        person.setName("name");
        country.setPersons(Set.of(person));

        // Run the test
        countryServiceImplUnderTest.update(country);

        // Verify the results
        verify(countryServiceImplUnderTest.countryRepository).save(any(Country.class));
    }

    @Test
    public void testDeleteById() {
        // Setup
        // Run the test
        countryServiceImplUnderTest.deleteById(0);

        // Verify the results
        verify(countryServiceImplUnderTest.countryRepository).deleteById(0);
    }
}