package com.nata.services;

import com.nata.model.User;
import com.nata.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsServiceImplUnderTest;

    @Test
    public void testLoadUserByUsername() {
        // Setup
        // Configure UserRepository.getUserByUserName(...).
        final User user = new User(0, "email", "fullName", "password", false);
        when(mockUserRepository.getUserByUserName("username")).thenReturn(user);

        // Run the test
        final UserDetails result = userDetailsServiceImplUnderTest.loadUserByUsername("username");

        // Verify the results
    }

    @Test
    public void testLoadUserByUsername_UserRepositoryReturnsNull() {
        // Setup
        when(mockUserRepository.getUserByUserName("username")).thenReturn(null);

        // Run the test
        assertThrows(UsernameNotFoundException.class,
                () -> userDetailsServiceImplUnderTest.loadUserByUsername("username"));
    }
}