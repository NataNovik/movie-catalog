function refreshData(url, inputId, pageSize, pageNo, totalItems, searchValue )
{
    pageSize = pageSize || 5;
    pageNo = (((pageNo - 1) * pageSize) < totalItems ? pageNo : Math.round(totalItems/pageSize)) || 1;
    $.ajax({
        type: 'get',
        url: url + "?pageNo=" + pageNo + "&pageSize="+pageSize  + (searchValue == null? "" : "&search="+searchValue) ,
        success: function(data){
            $('#' + inputId).replaceWith(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}