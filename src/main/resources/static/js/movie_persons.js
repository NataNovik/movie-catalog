function createListBox (itemName, valueName, tabNameText, rightTabNameText, searchPlaceholderText, dataArray, selectedDataArray, inputId)
{
    let settings = {
        // data item name
        itemName: itemName,
        // data value name
        valueName: valueName,
        // tab text
        tabNameText: tabNameText,
        // right tab text
        rightTabNameText: rightTabNameText,
        // search placeholder text
        searchPlaceholderText: searchPlaceholderText,
        // items data array
        dataArray: persons,
        // items data array
        selectedDataArray: selectedPersons,
        callable: function (dataArray) {
            let ids = dataArray.map(x => x.id);
            $("#"+inputId).val(ids);
        }
    };
    $("#persons-transfer").transfer(settings);
   let ids = JSON.parse(selectedPersons).map(x => x.id);

    $("#"+inputId).val(ids);
}