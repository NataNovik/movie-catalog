package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Collection.TABLE_NAME)
public class Collection {
    public static final String TABLE_NAME = "collection";
    public static final String COLUMN_ID_NAME = "collection_id";
    public static final String COLUMN_NAME_NAME = "name";

    private Integer id;

    private String name;

    private Set<Movie> movies = new LinkedHashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Integer getId() {
        return id;
    }

    public Collection setId(Integer id) {
        this.id = id;
        return this;
    }

    @Size(max = 80)
    @NotNull
    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 80)
    public String getName() {
        return name;
    }

    public Collection setName(String name) {
        this.name = name;
        return this;
    }

    @OneToMany(mappedBy = "collection")
    public Set<Movie> getMovies() {
        return movies;
    }

    public Collection setMovies(Set<Movie> movies) {
        this.movies = movies;
        return this;
    }

}