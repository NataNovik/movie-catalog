package com.nata.model;

import com.nata.model.Person;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = CrewCredit.TABLE_NAME)
public class CrewCredit {
    public static final String TABLE_NAME = "crew_credit";
    public static final String COLUMN_CREDITID_NAME = "credit_id";

    private String creditId;

    private Movie film;

    private Department department;

    private com.nata.model.Person person;

    private Job job;

    @Id
    @Size(max = 30)
    @Column(name = COLUMN_CREDITID_NAME, nullable = false, length = 30)
    public String getCreditId() {
        return creditId;
    }

    public CrewCredit setCreditId(String creditId) {
        this.creditId = creditId;
        return this;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "film_id", nullable = false)
    public Movie getFilm() {
        return film;
    }

    public CrewCredit setFilm(Movie film) {
        this.film = film;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    public Department getDepartment() {
        return department;
    }

    public CrewCredit setDepartment(Department department) {
        this.department = department;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    public com.nata.model.Person getPerson() {
        return person;
    }

    public CrewCredit setPerson(Person person) {
        this.person = person;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id")
    public Job getJob() {
        return job;
    }

    public CrewCredit setJob(Job job) {
        this.job = job;
        return this;
    }

}