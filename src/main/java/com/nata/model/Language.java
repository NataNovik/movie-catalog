package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Language.TABLE_NAME)
public class Language {
    public static final String TABLE_NAME = "language";
    public static final String COLUMN_CODE_NAME = "code";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String JOINTABLE_MOVIESWITHSPOKENLANGUAGE_NAME = "movie_spoken_language";
    public static final String JOINCOLUMNS_JOINCOLUMN_MOVIESWITHSPOKENLANGUAGE_NAME = "spoken_language_id";
    public static final String INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIESWITHSPOKENLANGUAGE_NAME = "film_id";


    private String code;

    private String name;

    private Set<Movie> moviesWithOriginalLanguage = new LinkedHashSet<>();

    private Set<Movie> moviesWithSpokenLanguage = new LinkedHashSet<>();

    @Id
    @Size(max = 10)
    @Column(name = COLUMN_CODE_NAME, nullable = false, length = 10)
    public String getCode() {
        return code;
    }

    public Language setCode(String code) {
        this.code = code;
        return this;
    }

    @Size(max = 30)
    @NotNull
    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 30)
    public String getName() {
        return name;
    }

    public Language setName(String name) {
        this.name = name;
        return this;
    }

    @OneToMany(mappedBy = "originalLanguageCode")
    public Set<Movie> getMoviesWithOriginalLanguage() {
        return moviesWithOriginalLanguage;
    }

    public Language setMoviesWithOriginalLanguage(Set<Movie> moviesWithOriginalLanguage) {
        this.moviesWithOriginalLanguage = moviesWithOriginalLanguage;
        return this;
    }

    @ManyToMany
    @JoinTable(name = JOINTABLE_MOVIESWITHSPOKENLANGUAGE_NAME,
            joinColumns = @JoinColumn(name = JOINCOLUMNS_JOINCOLUMN_MOVIESWITHSPOKENLANGUAGE_NAME),
            inverseJoinColumns = @JoinColumn(name = INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIESWITHSPOKENLANGUAGE_NAME))
    public Set<Movie> getMoviesWithSpokenLanguage() {
        return moviesWithSpokenLanguage;
    }

    public Language setMoviesWithSpokenLanguage(Set<Movie> moviesWithSpokenLanguage) {
        this.moviesWithSpokenLanguage = moviesWithSpokenLanguage;
        return this;
    }

}