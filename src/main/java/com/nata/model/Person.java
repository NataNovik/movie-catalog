package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Person.TABLE_NAME)
public class Person {
    public static final String TABLE_NAME = "person";
    public static final String COLUMN_ID_NAME = "person_id";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_BIRTHDAY_NAME = "birthday";
    public static final String COLUMN_ALSOKNOWNAS_NAME = "also_known_as";
    public static final String COLUMN_DEATHDAY_NAME = "deathday";
    public static final String COLUMN_BIOGRAPHY_NAME = "biography";
    public static final String COLUMN_PROFILEPATH_NAME = "profile_path";
    public static final String COLUMN_POPULARITY_NAME = "popularity";
    public static final String COLUMN_PLACEOFBIRTH_NAME = "place_of_birth";
    public static final String COLUMN_IMDBID_NAME = "imdb_id";
    public static final String COLUMN_HOMEPAGE_NAME = "homepage";
    public static final String COLUMN_ADDITIONALINFO_NAME = "additional_info";


    private Integer id;

    private String name;

    private Department department;

    private LocalDate birthday;

    private String alsoKnownAs;

    private LocalDate deathday;

    private Gender gender;

    private String biography;

    private String profilePath;

    private BigDecimal popularity;

    private String placeOfBirth;

    private String imdbId;

    private String homepage;

    private Country countryCode;

    private String additionalInfo;

    private Set<CrewCredit> crewCredits = new LinkedHashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Integer getId() {
        return id;
    }

    public Person setId(Integer id) {
        this.id = id;
        return this;
    }

    @Size(max = 50)
    @NotNull
    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    public Department getDepartment() {
        return department;
    }

    public Person setDepartment(Department department) {
        this.department = department;
        return this;
    }

    @Column(name = COLUMN_BIRTHDAY_NAME)
    public LocalDate getBirthday() {
        return birthday;
    }

    public Person setBirthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    @Size(max = 1000)
    @Column(name = COLUMN_ALSOKNOWNAS_NAME, length = 1000)
    public String getAlsoKnownAs() {
        return alsoKnownAs;
    }

    public Person setAlsoKnownAs(String alsoKnownAs) {
        this.alsoKnownAs = alsoKnownAs;
        return this;
    }

    @Column(name = COLUMN_DEATHDAY_NAME)
    public LocalDate getDeathday() {
        return deathday;
    }

    public Person setDeathday(LocalDate deathday) {
        this.deathday = deathday;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gender_id")
    public Gender getGender() {
        return gender;
    }

    public Person setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    @Lob
    @Column(name = COLUMN_BIOGRAPHY_NAME)
    public String getBiography() {
        return biography;
    }

    public Person setBiography(String biography) {
        this.biography = biography;
        return this;
    }

    @Size(max = 40)
    @Column(name = COLUMN_PROFILEPATH_NAME, length = 40)
    public String getProfilePath() {
        return profilePath;
    }

    public Person setProfilePath(String profilePath) {
        this.profilePath = profilePath;
        return this;
    }

    @NotNull
    @Column(name = COLUMN_POPULARITY_NAME, nullable = false, precision = 20, scale = 6)
    public BigDecimal getPopularity() {
        return popularity;
    }

    public Person setPopularity(BigDecimal popularity) {
        this.popularity = popularity;
        return this;
    }

    @Size(max = 100)
    @Column(name = COLUMN_PLACEOFBIRTH_NAME, length = 100)
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public Person setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    @Size(max = 10)
    @Column(name = COLUMN_IMDBID_NAME, length = 10)
    public String getImdbId() {
        return imdbId;
    }

    public Person setImdbId(String imdbId) {
        this.imdbId = imdbId;
        return this;
    }

    @Size(max = 160)
    @Column(name = COLUMN_HOMEPAGE_NAME, length = 160)
    public String getHomepage() {
        return homepage;
    }

    public Person setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_code")
    @NotFound(action = NotFoundAction.IGNORE)
    public Country getCountryCode() {
        return countryCode;
    }

    public Person setCountryCode(Country countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    @Size(max = 200)
    @Column(name = COLUMN_ADDITIONALINFO_NAME, length = 200)
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public Person setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    @OneToMany(mappedBy = "person")
    public Set<CrewCredit> getCrewCredits() {
        return crewCredits;
    }

    public Person setCrewCredits(Set<CrewCredit> crewCredits) {
        this.crewCredits = crewCredits;
        return this;
    }

}