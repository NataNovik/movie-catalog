package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Movie.TABLE_NAME)
public class Movie {
    public static final String TABLE_NAME = "movie";
    public static final String COLUMN_ID_NAME = "film_id";
    public static final String COLUMN_TITLE_NAME = "title";
    public static final String COLUMN_BACKDROPPATH_NAME = "backdrop_path";
    public static final String COLUMN_BUDGET_NAME = "budget";
    public static final String COLUMN_HOMEPAGE_NAME = "homepage";
    public static final String COLUMN_IMDBID_NAME = "imdb_id";
    public static final String COLUMN_ORIGINALTITLE_NAME = "original_title";
    public static final String COLUMN_OVERVIEW_NAME = "overview";
    public static final String COLUMN_POPULARITY_NAME = "popularity";
    public static final String COLUMN_POSTERPATH_NAME = "poster_path";
    public static final String COLUMN_RELEASEDATE_NAME = "release_date";
    public static final String COLUMN_REVENUE_NAME = "revenue";
    public static final String COLUMN_RUNTIME_NAME = "runtime";
    public static final String COLUMN_VOTEAVERAGE_NAME = "vote_average";
    public static final String COLUMN_VOTECOUNT_NAME = "vote_count";
    public static final String COLUMN_STATUS_NAME = "status";
    public static final String COLUMN_TAGLINE_NAME = "tagline";
    public static final String COLUMN_TRAILERLINK_NAME = "trailer_link";


    private Integer id;

    private String title;

    private String backdropPath;

    private Integer budget;

    private String homepage;

    private String imdbId;

    private Language originalLanguageCode;

    private String originalTitle;

    private String overview;

    private BigDecimal popularity;

    private String posterPath;

    private LocalDate releaseDate;

    private Long revenue;

    private String runtime;

    private BigDecimal voteAverage;

    private Short voteCount;

    private String status;

    private String tagline;

    private Collection collection;

    private String trailerLink;

    private Set<CrewCredit> crewCredits = new LinkedHashSet<>();

//    private Set<Kudagomovie> kudagomovies = new LinkedHashSet<>();

    private Set<Company> companies = new LinkedHashSet<>();

    private Set<Genre> genres = new LinkedHashSet<>();

    private Set<String> movieKeywords = new LinkedHashSet<>();

    private Set<Movie> moviesInRecommendations = new LinkedHashSet<>();

    private Set<Movie> includedInRecommendations = new LinkedHashSet<>();

    private Set<MovieReview> movieReviews = new LinkedHashSet<>();

    private Set<Movie> moviesInSimilars = new LinkedHashSet<>();

    private Set<Movie> includedInSimilars = new LinkedHashSet<>();

    private Set<Language> spokenLanguages = new LinkedHashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Integer getId() {
        return id;
    }

    public Movie setId(Integer id) {
        this.id = id;
        return this;
    }

    @Size(max = 120)
    @NotNull
    @Column(name = COLUMN_TITLE_NAME, nullable = false, length = 120)
    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    @Size(max = 40)
    @Column(name = COLUMN_BACKDROPPATH_NAME, length = 40)
    public String getBackdropPath() {
        return backdropPath;
    }

    public Movie setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
        return this;
    }

    @Column(name = COLUMN_BUDGET_NAME)
    public Integer getBudget() {
        return budget;
    }

    public Movie setBudget(Integer budget) {
        this.budget = budget;
        return this;
    }

    @Lob
    @Column(name = COLUMN_HOMEPAGE_NAME)
    public String getHomepage() {
        return homepage;
    }

    public Movie setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    @Size(max = 10)
    @Column(name = COLUMN_IMDBID_NAME, length = 10)
    public String getImdbId() {
        return imdbId;
    }

    public Movie setImdbId(String imdbId) {
        this.imdbId = imdbId;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "original_language_code")
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(FetchMode.JOIN)
    public Language getOriginalLanguageCode() {
        return originalLanguageCode;
    }

    public Movie setOriginalLanguageCode(Language originalLanguageCode) {
        this.originalLanguageCode = originalLanguageCode;
        return this;
    }

    @Size(max = 120)
    @Column(name = COLUMN_ORIGINALTITLE_NAME, nullable = false, length = 120)
    public String getOriginalTitle() {
        return originalTitle;
    }

    public Movie setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
        return this;
    }

    @Lob
    @Column(name = COLUMN_OVERVIEW_NAME)
    public String getOverview() {
        return overview;
    }

    public Movie setOverview(String overview) {
        this.overview = overview;
        return this;
    }

    @Column(name = COLUMN_POPULARITY_NAME, nullable = false, precision = 20, scale = 6)
    public BigDecimal getPopularity() {
        return popularity;
    }

    public Movie setPopularity(BigDecimal popularity) {
        this.popularity = popularity;
        return this;
    }

    @Size(max = 40)
    @Column(name = COLUMN_POSTERPATH_NAME, length = 40)
    public String getPosterPath() {
        return posterPath;
    }

    public Movie setPosterPath(String posterPath) {
        this.posterPath = posterPath;
        return this;
    }

    @Column(name = COLUMN_RELEASEDATE_NAME, nullable = false)
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Movie setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    @Column(name = COLUMN_REVENUE_NAME)
    public Long getRevenue() {
        return revenue;
    }

    public Movie setRevenue(Long revenue) {
        this.revenue = revenue;
        return this;
    }

    @Size(max = 10)
    @Column(name = COLUMN_RUNTIME_NAME, length = 10)
    public String getRuntime() {
        return runtime;
    }

    public Movie setRuntime(String runtime) {
        this.runtime = runtime;
        return this;
    }

    @Column(name = COLUMN_VOTEAVERAGE_NAME, precision = 20, scale = 6)
    public BigDecimal getVoteAverage() {
        return voteAverage;
    }

    public Movie setVoteAverage(BigDecimal voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    @Column(name = COLUMN_VOTECOUNT_NAME)
    public Short getVoteCount() {
        return voteCount;
    }

    public Movie setVoteCount(Short voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    @Size(max = 20)
    @Column(name = COLUMN_STATUS_NAME, nullable = false, length = 20)
    public String getStatus() {
        return status;
    }

    public Movie setStatus(String status) {
        this.status = status;
        return this;
    }

    @Lob
    @Column(name = COLUMN_TAGLINE_NAME)
    public String getTagline() {
        return tagline;
    }

    public Movie setTagline(String tagline) {
        this.tagline = tagline;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collection_id")
    public Collection getCollection() {
        return collection;
    }

    public Movie setCollection(Collection collection) {
        this.collection = collection;
        return this;
    }

    @Size(max = 500)
    @Column(name = COLUMN_TRAILERLINK_NAME, length = 500)
    public String getTrailerLink() {
        return trailerLink;
    }

    public Movie setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
        return this;
    }

    @OneToMany(mappedBy = "film")
    public Set<CrewCredit> getCrewCredits() {
        return crewCredits;
    }

    public Movie setCrewCredits(Set<CrewCredit> crewCredits) {
        this.crewCredits = crewCredits;
        return this;
    }

    @ManyToMany(mappedBy = "movies")
    public Set<Company> getCompanies() {
        return companies;
    }

    public Movie setCompanies(Set<Company> companies) {
        this.companies = companies;
        return this;
    }

    @ManyToMany(mappedBy = "movies")
    @Fetch(FetchMode.JOIN)
    public Set<Genre> getGenres() {
        return genres;
    }

    public Movie setGenres(Set<Genre> genres) {
        this.genres = genres;
        return this;
    }

    @ElementCollection
    @CollectionTable(
            name="movie_keyword",
            joinColumns=@JoinColumn(name="film_id"))
    @Column(name = "keyword")
    public Set<String> getMovieKeywords() {
        return movieKeywords;
    }

    public Movie setMovieKeywords(Set<String> movieKeywords) {
        this.movieKeywords = movieKeywords;
        return this;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "movie_recommend", joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "recommended_film_id", referencedColumnName = "film_id"))
    public Set<Movie> getMoviesInRecommendations() {
        return moviesInRecommendations;
    }

    public Movie setMoviesInRecommendations(Set<Movie> moviesInRecommendations) {
        this.moviesInRecommendations = moviesInRecommendations;
        return this;
    }

    @ManyToMany(mappedBy = "moviesInRecommendations")
    public Set<Movie> getIncludedInRecommendations() {
        return includedInRecommendations;
    }

    public Movie setIncludedInRecommendations(Set<Movie> includedInRecommendations) {
        this.includedInRecommendations = includedInRecommendations;
        return this;
    }

    @OneToMany(mappedBy = "film")
    public Set<MovieReview> getMovieReviews() {
        return movieReviews;
    }

    public Movie setMovieReviews(Set<MovieReview> movieReviews) {
        this.movieReviews = movieReviews;
        return this;
    }

    @ManyToMany(mappedBy = "includedInSimilars")
    public Set<Movie> getMoviesInSimilars() {
        return moviesInSimilars;
    }

    public Movie setMoviesInSimilars(Set<Movie> moviesInSimilars) {
        this.moviesInSimilars = moviesInSimilars;
        return this;
    }

    @ManyToMany()
    @JoinTable(name = "movie_similar", joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "film_id"), inverseJoinColumns = @JoinColumn(name = "similar_film_id", referencedColumnName = "film_id"))
    public Set<Movie> getIncludedInSimilars() {
        return includedInSimilars;
    }

    public Movie setIncludedInSimilars(Set<Movie> includedInSimilars) {
        this.includedInSimilars = includedInSimilars;
        return this;
    }

    @ManyToMany(mappedBy = "moviesWithSpokenLanguage")
    public Set<Language> getSpokenLanguages() {
        return spokenLanguages;
    }

    public Movie setSpokenLanguages(Set<Language> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
        return this;
    }

}