package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = MovieReview.TABLE_NAME)
public class MovieReview {
    public static final String TABLE_NAME = "movie_review";
    public static final String COLUMN_ID_NAME = "movie_review_id";
    public static final String COLUMN_AUTHOR_NAME = "author";
    public static final String COLUMN_COMMENTID_NAME = "comment_id";
    public static final String COLUMN_URL_NAME = "url";
    public static final String COLUMN_CONTENT_NAME = "content";


    private Short id;

    private Movie film;

    private String author;

    private String commentId;

    private String url;

    private String content;

    @Id
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Short getId() {
        return id;
    }

    public MovieReview setId(Short id) {
        this.id = id;
        return this;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "film_id", nullable = false)
    public Movie getFilm() {
        return film;
    }

    public MovieReview setFilm(Movie film) {
        this.film = film;
        return this;
    }

    @Size(max = 70)
    @Column(name = COLUMN_AUTHOR_NAME, length = 70)
    public String getAuthor() {
        return author;
    }

    public MovieReview setAuthor(String author) {
        this.author = author;
        return this;
    }

    @Size(max = 30)
    @Column(name = COLUMN_COMMENTID_NAME, length = 30)
    public String getCommentId() {
        return commentId;
    }

    public MovieReview setCommentId(String commentId) {
        this.commentId = commentId;
        return this;
    }

    @Size(max = 60)
    @Column(name = COLUMN_URL_NAME, length = 60)
    public String getUrl() {
        return url;
    }

    public MovieReview setUrl(String url) {
        this.url = url;
        return this;
    }

    @Lob
    @Column(name = COLUMN_CONTENT_NAME)
    public String getContent() {
        return content;
    }

    public MovieReview setContent(String content) {
        this.content = content;
        return this;
    }

}