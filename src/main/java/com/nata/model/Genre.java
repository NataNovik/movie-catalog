package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Genre.TABLE_NAME)
public class Genre {
    public static final String TABLE_NAME = "genre";
    public static final String COLUMN_ID_NAME = "genre_id";
    public static final String COLUMN_GENRE_NAME = "genre";
    public static final String JOINTABLE_MOVIES_NAME = "movie_genre";
    public static final String JOINCOLUMNS_JOINCOLUMN_MOVIES_NAME = "genre_id";
    public static final String INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIES_NAME = "film_id";


    private Short id;

    private String genre;

    private Set<Movie> movies = new LinkedHashSet<>();

    @Id
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Short getId() {
        return id;
    }

    public Genre setId(Short id) {
        this.id = id;
        return this;
    }

    @Size(max = 20)
    @NotNull
    @Column(name = COLUMN_GENRE_NAME, nullable = false, length = 20)
    public String getGenre() {
        return genre;
    }

    public Genre setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    @ManyToMany
    @JoinTable(name = JOINTABLE_MOVIES_NAME,
            joinColumns = @JoinColumn(name = JOINCOLUMNS_JOINCOLUMN_MOVIES_NAME),
            inverseJoinColumns = @JoinColumn(name = INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIES_NAME))
    public Set<Movie> getMovies() {
        return movies;
    }

    public Genre setMovies(Set<Movie> movies) {
        this.movies = movies;
        return this;
    }

}