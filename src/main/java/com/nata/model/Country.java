package com.nata.model;

import com.nata.model.Person;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Country.TABLE_NAME)
public class Country  {
    public static final String TABLE_NAME = "country";
    public static final String COLUMN_CODE_NAME = "code";
    public static final String COLUMN_COUNTRY_NAME = "country";

    private String code;

    private String country;

    private Set<com.nata.model.Person> persons = new LinkedHashSet<>();

    @Id
    @Size(max = 2)
    @Column(name = COLUMN_CODE_NAME, nullable = false, length = 2)
    public String getCode() {
        return code;
    }

    public Country setCode(String code) {
        this.code = code;
        return this;
    }

    @Size(max = 50)
    @NotNull
    @Column(name = COLUMN_COUNTRY_NAME, nullable = false, length = 50)
    public String getCountry() {
        return country;
    }

    public Country setCountry(String country) {
        this.country = country;
        return this;
    }

    @OneToMany(mappedBy = "countryCode")
    public Set<com.nata.model.Person> getPersons() {
        return persons;
    }

    public Country setPersons(Set<Person> persons) {
        this.persons = persons;
        return this;
    }

}