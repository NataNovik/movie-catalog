package com.nata.model;

import com.nata.model.Person;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Department.TABLE_NAME)
public class Department {
    public static final String TABLE_NAME = "department";
    public static final String COLUMN_ID_NAME = "department_id";
    public static final String COLUMN_DEPARTMENT_NAME = "department";

    private Byte id;

    private String department;

    private Set<CrewCredit> crewCredits = new LinkedHashSet<>();

    private Set<Job> jobs = new LinkedHashSet<>();

    private Set<com.nata.model.Person> people = new LinkedHashSet<>();

    @Id
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Byte getId() {
        return id;
    }

    public Department setId(Byte id) {
        this.id = id;
        return this;
    }

    @Size(max = 20)
    @NotNull
    @Column(name = COLUMN_DEPARTMENT_NAME, nullable = false, length = 20)
    public String getDepartment() {
        return department;
    }

    public Department setDepartment(String department) {
        this.department = department;
        return this;
    }

    @OneToMany(mappedBy = "department")
    public Set<CrewCredit> getCrewCredits() {
        return crewCredits;
    }

    public Department setCrewCredits(Set<CrewCredit> crewCredits) {
        this.crewCredits = crewCredits;
        return this;
    }

    @OneToMany(mappedBy = "department")
    public Set<Job> getJobs() {
        return jobs;
    }

    public Department setJobs(Set<Job> jobs) {
        this.jobs = jobs;
        return this;
    }

    @OneToMany(mappedBy = "department")
    public Set<com.nata.model.Person> getPeople() {
        return people;
    }

    public Department setPeople(Set<Person> people) {
        this.people = people;
        return this;
    }

}