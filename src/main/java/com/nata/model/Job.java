package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Job.TABLE_NAME)
public class Job {
    public static final String TABLE_NAME = "job";
    public static final String COLUMN_ID_NAME = "job_id";
    public static final String COLUMN_JOB_NAME = "job";

    private Short id;

    private String job;

    private Department department;

    private Set<CrewCredit> crewCredits = new LinkedHashSet<>();

    @Id
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Short getId() {
        return id;
    }

    public Job setId(Short id) {
        this.id = id;
        return this;
    }

    @Size(max = 50)
    @NotNull
    @Column(name = COLUMN_JOB_NAME, nullable = false, length = 50)
    public String getJob() {
        return job;
    }

    public Job setJob(String job) {
        this.job = job;
        return this;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "department_id", nullable = false)
    public Department getDepartment() {
        return department;
    }

    public Job setDepartment(Department department) {
        this.department = department;
        return this;
    }

    @OneToMany(mappedBy = "job")
    public Set<CrewCredit> getCrewCredits() {
        return crewCredits;
    }

    public Job setCrewCredits(Set<CrewCredit> crewCredits) {
        this.crewCredits = crewCredits;
        return this;
    }

}