package com.nata.model;

import com.nata.model.Person;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Gender.TABLE_NAME)
public class Gender {
    public static final String TABLE_NAME = "gender";
    public static final String COLUMN_ID_NAME = "gender_id";
    public static final String COLUMN_GENDERNAME_NAME = "gender_name";

    private Byte id;

    private String genderName;

    private Set<Person> people = new LinkedHashSet<>();

    @Id
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Byte getId() {
        return id;
    }

    public Gender setId(Byte id) {
        this.id = id;
        return this;
    }

    @Size(max = 50)
    @NotNull
    @Column(name = COLUMN_GENDERNAME_NAME, nullable = false, length = 50)
    public String getGenderName() {
        return genderName;
    }

    public Gender setGenderName(String genderName) {
        this.genderName = genderName;
        return this;
    }

    @OneToMany(mappedBy = "gender")
    public Set<Person> getPeople() {
        return people;
    }

    public Gender setPeople(Set<Person> people) {
        this.people = people;
        return this;
    }

}