package com.nata.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = Company.TABLE_NAME)
public class Company {
    public static final String TABLE_NAME = "company";
    public static final String COLUMN_ID_NAME = "company_id";
    public static final String COLUMN_HOMEPAGE_NAME = "homepage";
    public static final String COLUMN_DESCRIPTION_NAME = "description";
    public static final String COLUMN_HEADQUERTERS_NAME = "headquerters";
    public static final String COLUMN_LOGOPATH_NAME = "logo_path";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_ORIGINCOUNTRYCODE_NAME = "origin_country_code";
    public static final String JOINTABLE_MOVIES_NAME = "movie_company";
    public static final String JOINCOLUMNS_JOINCOLUMN_MOVIES_NAME = "company_id";
    public static final String INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIES_NAME = "film_id";


    private Integer id;

    private String homepage;

    private String description;

    private String headquerters;

    private String logoPath;

    private String name;

    private String originCountryCode;

    private Company parentCompany;

    private Set<Company> childCompanies = new LinkedHashSet<>();

    private Set<Movie> movies = new LinkedHashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID_NAME, nullable = false)
    public Integer getId() {
        return id;
    }

    public Company setId(Integer id) {
        this.id = id;
        return this;
    }

    @Size(max = 180)
    @Column(name = COLUMN_HOMEPAGE_NAME, length = 180)
    public String getHomepage() {
        return homepage;
    }

    public Company setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    @Size(max = 2000)
    @Column(name = COLUMN_DESCRIPTION_NAME, length = 2000)
    public String getDescription() {
        return description;
    }

    public Company setDescription(String description) {
        this.description = description;
        return this;
    }

    @Size(max = 90)
    @Column(name = COLUMN_HEADQUERTERS_NAME, length = 90)
    public String getHeadquerters() {
        return headquerters;
    }

    public Company setHeadquerters(String headquerters) {
        this.headquerters = headquerters;
        return this;
    }

    @Size(max = 40)
    @Column(name = COLUMN_LOGOPATH_NAME, length = 40)
    public String getLogoPath() {
        return logoPath;
    }

    public Company setLogoPath(String logoPath) {
        this.logoPath = logoPath;
        return this;
    }

    @Size(max = 110)
    @NotNull
    @Column(name = COLUMN_NAME_NAME, nullable = false, length = 110)
    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;
        return this;
    }

    @Size(max = 2)
    @Column(name = COLUMN_ORIGINCOUNTRYCODE_NAME, length = 2)
    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public Company setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_company_id")
    public Company getParentCompany() {
        return parentCompany;
    }

    public Company setParentCompany(Company parentCompany) {
        this.parentCompany = parentCompany;
        return this;
    }

    @OneToMany(mappedBy = "parentCompany")
    public Set<Company> getChildCompanies() {
        return childCompanies;
    }

    public Company setChildCompanies(Set<Company> childCompanies) {
        this.childCompanies = childCompanies;
        return this;
    }

    @ManyToMany
    @JoinTable(name = JOINTABLE_MOVIES_NAME,
            joinColumns = @JoinColumn(name = JOINCOLUMNS_JOINCOLUMN_MOVIES_NAME),
            inverseJoinColumns = @JoinColumn(name = INVERSEJOINCOLUMNS_JOINCOLUMN_MOVIES_NAME))
    public Set<Movie> getMovies() {
        return movies;
    }

    public Company setMovies(Set<Movie> movies) {
        this.movies = movies;
        return this;
    }

}