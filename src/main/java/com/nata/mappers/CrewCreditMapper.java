package com.nata.mappers;

import com.nata.dto.CrewCreditDto;
import com.nata.model.CrewCredit;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface CrewCreditMapper {
    CrewCredit toEntity(CrewCreditDto crewCreditDto);

    CrewCreditDto toDto(CrewCredit crewCredit);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CrewCredit partialUpdate(CrewCreditDto crewCreditDto, @MappingTarget CrewCredit crewCredit);
}