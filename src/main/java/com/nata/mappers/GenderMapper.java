package com.nata.mappers;

import com.nata.dto.GenderDto;
import com.nata.model.Gender;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface GenderMapper {
    Gender toEntity(GenderDto genderDto);

    GenderDto toDto(Gender gender);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Gender partialUpdate(GenderDto genderDto, @MappingTarget Gender gender);
}