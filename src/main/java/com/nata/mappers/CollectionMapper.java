package com.nata.mappers;

import com.nata.dto.CollectionDto;
import com.nata.model.Collection;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface CollectionMapper {
    Collection toEntity(CollectionDto collectionDto);

    @AfterMapping
    default void linkMovies(@MappingTarget Collection collection) {
        collection.getMovies().forEach(movie -> movie.setCollection(collection));
    }

    CollectionDto toDto(Collection collection);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Collection partialUpdate(CollectionDto collectionDto, @MappingTarget Collection collection);
}