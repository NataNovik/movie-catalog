package com.nata.mappers;

import com.nata.dto.MovieDto;
import com.nata.model.Movie;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface MovieMapper {
    Movie toEntity(MovieDto movieDto);

    @AfterMapping
    default void linkMovieReviews(@MappingTarget Movie movie) {
        movie.getMovieReviews().forEach(movieReview -> movieReview.setFilm(movie));
    }

//    @AfterMapping
//    protected void ignoreFathersChildren(Child child, @MappingTarget ChildDto childDto) {
//        childDto.getFather().setChildren(null);
//    }

    MovieDto toDto(Movie movie);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Movie partialUpdate(MovieDto movieDto, @MappingTarget Movie movie);
}