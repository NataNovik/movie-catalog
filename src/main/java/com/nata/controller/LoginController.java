package com.nata.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginView() {
        return "common/login_page";
    }
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Optional<String> message) {
        return "common/logout_page";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute Object login) {
        return "common/home";
    }
    @RequestMapping(value = "/403", method = RequestMethod.POST)
    public String DenyAccess(@ModelAttribute Object login) {
        return "common/no_access";
    }
}