package com.nata.controller;

import com.nata.services.PersonService;
import com.nata.services.CountryService;
import com.nata.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Controller
public class HomeController {
    @Autowired
    MovieService movieService;
    @Autowired
    PersonService personService;
    @Autowired
    CountryService countryService;

    @GetMapping(value = {  "/home", "/*", "/" })
    public String selectAll(Model model, Optional<String> message) {
        model.addAttribute("message", message);
        return "common/home";
    }
}