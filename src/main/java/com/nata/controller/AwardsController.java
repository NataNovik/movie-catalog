package com.nata.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class AwardsController {
    @GetMapping("awards")
    public String  selectAll(){
        return "awards/awards";
    }
}