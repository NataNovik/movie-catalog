package com.nata.controller;


import com.nata.model.User;
import com.nata.repository.UserRepository;
import com.nata.services.CountryService;
import com.nata.services.MovieService;
import com.nata.services.UserService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

import static com.nata.controller.UserController.*;


@CrossOrigin(origins = "*")
@Controller
@SessionAttributes({ModelAttr.CURRENT_PAGE, ModelAttr.TOTAL_PAGES, ModelAttr.TOTAL_ITEMS, ModelAttr.PAGE_SIZE})
public class UserController {
    final
    UserService userService;
    final
    CountryService countryService;
    final
    MovieService movieService;
    final
    UserRepository userRepository;

    public UserController(UserService userService, CountryService countryService, MovieService movieService, UserRepository userRepository) {
        this.userService = userService;
        this.countryService = countryService;
        this.movieService = movieService;
        this.userRepository = userRepository;
    }
    
    @ModelAttribute(ModelAttr.CURRENT_PAGE)
    public Integer currentPage() {
        return Default.PAGE_NO;
    }

    @ModelAttribute(ModelAttr.TOTAL_PAGES)
    public Integer totalPages() {
        return 1;
    }

    @ModelAttribute(ModelAttr.TOTAL_ITEMS)
    public Integer totalItems() {
        return 1;
    }

    @ModelAttribute(ModelAttr.PAGE_SIZE)
    public Integer pageSize() {
        return Default.PAGE_SIZE;
    }

    //region sign up
    @GetMapping(URL.SIGN_UP)
    public String registerUser() {
        return TemplatePath.ADD_FORM;
    }

    @PostMapping(URL.SIGN_UP)
    public ResponseEntity<?> registerUser(String signUpRequest) {

        return new ResponseEntity<>(Message.UserRegisteredSuccessfully, HttpStatus.OK);
    }
    //endregion

    //region edit profile
    @GetMapping(URL.PROFILE)
    public String editProfile(Model model) {
        final String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
        var user =  userService.getUserByUserName(currentUserName);
        model.addAttribute(ModelAttr.OBJ, user);
        return TemplatePath.EDIT_FORM;

    }
    //endregion

    //region edit user
    @GetMapping(URL.EDIT)
    public String editUser(@RequestParam int id, Model model) {
        User user = userService.findById(id);
        model.addAttribute(ModelAttr.OBJ, user);
        return TemplatePath.EDIT_FORM;
    }

    @PostMapping(URL.EDIT)
    public String updateUser(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return TemplatePath.EDIT_FORM;
        }

        userService.update(user);

        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region create user
    @GetMapping(URL.CREATE)
    public String createUser(Model model) {
        var user = new User();
        model.addAttribute(ModelAttr.OBJ, user);
        return TemplatePath.ADD_FORM;
    }


    @PostMapping(URL.CREATE)
    public String createUser(@Valid User user, BindingResult result, Model model) {
//                             RequestParam("role_id") Integer roleId

        if (result.hasErrors()) {
            model.addAttribute(ModelAttr.ERRORS, result.getAllErrors());
            return TemplatePath.ADD_FORM;
        }
        userService.update(user);
        model.addAttribute(ModelAttr.USERS, userRepository.findAll());
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region delete user
    @GetMapping(URL.DELETE)
    public String deleteUser(@RequestParam int id,
                             Model model, @SessionAttribute Optional<Integer> totalItems) {
        if (totalItems.isPresent()) {
            model.addAttribute(ModelAttr.TOTAL_ITEMS, totalItems.get() - 1);
        }
        userService.deleteById(id);
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region select users
    @GetMapping(URL.SELECT_ALL)
    public String selectAll(Model model, @SessionAttribute(MovieController.ModelAttr.CURRENT_PAGE) Optional<Integer> pageNo,
        @SessionAttribute(MovieController.ModelAttr.PAGE_SIZE) Optional<Integer> pageSize,
        @SessionAttribute(MovieController.ModelAttr.SEARCH) Optional<String> search) {
            fillModelForGrid(pageNo, pageSize, search, model);
        return TemplatePath.LIST;
    }


    @GetMapping(URL.REFRESH_LIST_DATA)
    public ModelAndView selectAllData(@RequestParam(Param.PAGE_NO) Optional<Integer> pageNo,
                                      @RequestParam(Param.PAGE_SIZE) Optional<Integer> pageSize,
                                      @RequestParam(Param.SEARCH) Optional<String> search,
                                      Model model) {
        fillModelForGrid(pageNo, pageSize, search, model);
        return new ModelAndView(TemplatePath.LIST_DATA, model.asMap());
    }

    private void fillModelForGrid(Optional<Integer> pageNo, Optional<Integer> pageSize, Optional<String> search, Model model) {
        if (pageNo.isEmpty()) {
            pageNo = Optional.of(Default.PAGE_NO);
        }
        if (pageSize.isEmpty()) {
            pageSize = Optional.of(Default.PAGE_SIZE);
        }
        Page<User> page;
        if (search.isEmpty()) {
            page = userService.findPaginated(pageNo.get(), pageSize.get());
        } else {
            page = userService.findPaginatedWithFilterByFullNameIgnoreCase(pageNo.get(), pageSize.get(), search.get());
        }
        long totalItems = page.getTotalElements();
        int totalPagesCount = page.getTotalPages();
        model.addAttribute(ModelAttr.TOTAL_PAGES, totalPagesCount);
        model.addAttribute(ModelAttr.CURRENT_PAGE, pageNo.get() > totalPagesCount ? totalPagesCount : pageNo.get());
        model.addAttribute(ModelAttr.TOTAL_ITEMS, totalItems);
        model.addAttribute(ModelAttr.PAGE_SIZE, pageSize.get());
        model.addAttribute(ModelAttr.LIST, page.getContent());
        model.addAttribute(ModelAttr.SEARCH, search.orElse(null));
    }
    //endregion

    //region constants
    static final class Message
    {
        static final String UserRegisteredSuccessfully = "User registered Successfully!";
    }


    static final class URL {
        static final String CREATE = "create_user";
        static final String SIGN_UP = "signup";
        static final String SELECT_ALL = "users";
        static final String PROFILE = "profile";
        static final String REFRESH_LIST_DATA = "refresh_users";
        static final String EDIT= "edit_user";
        static final String DELETE = "delete_user";
        static final String REDIRECT_TO_LIST = "redirect:/users";
    }

    static final class TemplatePath {
        static final String LIST = "user/user_list";
        static final String LIST_DATA = "user/user_list_data";
        static final String EDIT_FORM = "user/user_edit_form";
        static final String ADD_FORM = "user/user_add_form";
    }

    static final class ModelAttr {
        public static final String ERRORS = "errors";
        static final String CURRENT_PAGE = "user_currentPage";
        static final String TOTAL_PAGES = "user_totalPages";
        static final String TOTAL_ITEMS = "user_totalItems";
        static final String PAGE_SIZE = "user_pageSize";
        static final String LIST = "user_list";
        static final String SEARCH = "user_search";
        static final String COUNTRIES = "countries";
        static final String USERS = "USERS";
        static final String SELECTED_PERSONS = "selectedPersons";
        static final String GENDERS = "genders";
        static final String DEPARTMENTS = "departments";
        static final String OBJ = "obj";
    }

    static final class Default {
        static final int PAGE_NO = 1;
        static final int PAGE_SIZE = 10;
    }

    static final class Param {
        static final String PAGE_NO = "pageNo";
        static final String PAGE_SIZE = "pageSize";
        static final String SEARCH = "search";
    }
    //endregion
}