package com.nata.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ErrController implements ErrorController {
    private static final String PATH = "error";
    @RequestMapping(PATH)
    public String handleError(HttpServletRequest request, Model model ) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        model.addAttribute("error", status != null ? status.toString() : "");
        return "common/error";
    }
}