package com.nata.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class QuotesController {
    @GetMapping("quotes")
    public String selectAll(){
       return "quotes/quotes_list";
    }
}