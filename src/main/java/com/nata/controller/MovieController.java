package com.nata.controller;

import com.nata.dto.PersonDto;
import com.nata.dto.MovieDto;
import com.nata.mappers.LanguageMapper;
import com.nata.mappers.MovieMapper;
import com.nata.mappers.PersonMapper;
import com.nata.model.Movie;
import com.nata.repository.LanguageRepository;
import com.nata.repository.MovieRepository;
import com.nata.services.PersonService;
import com.nata.services.CountryService;
import com.nata.services.MovieService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.nata.controller.MovieController.*;

@Controller
@SessionAttributes({ModelAttr.CURRENT_PAGE, ModelAttr.TOTAL_PAGES, ModelAttr.TOTAL_ITEMS, ModelAttr.PAGE_SIZE, ModelAttr.SEARCH})
public class MovieController {

    @ModelAttribute(ModelAttr.CURRENT_PAGE)
    public Integer getCurrentPage() {
        return Default.PAGE_NO; }
    @ModelAttribute(ModelAttr.TOTAL_PAGES)
    public Integer getTotalPages() { return 1; }
    @ModelAttribute(ModelAttr.TOTAL_ITEMS)
    public Integer getTotalItems() { return 1; }
    @ModelAttribute(ModelAttr.PAGE_SIZE)
    public Integer getPageSize() { return Default.PAGE_SIZE; }
    @ModelAttribute(ModelAttr.SEARCH)
    public String getSearch() { return ""; }

    Logger logger = LoggerFactory.getLogger(MovieController.class);
    final
    MovieService movieService;
    final
    PersonService personService;
    final
    CountryService countryService;
    final
    MovieRepository movieRepository;
    final
    MovieMapper movieMapper;
    final
    PersonMapper personMapper;
    private final LanguageRepository languageRepository;
    private final LanguageMapper languageMapper;

    public MovieController(MovieService movieService, PersonService personService, CountryService countryService,
                           MovieRepository movieRepository, MovieMapper movieMapper, PersonMapper personMapper,
                           LanguageRepository languageRepository,
                           LanguageMapper languageMapper) {
        this.movieService = movieService;
        this.personService = personService;
        this.countryService = countryService;
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
        this.personMapper = personMapper;
        this.languageRepository = languageRepository;
        this.languageMapper = languageMapper;
    }



    //region edit movie
    @GetMapping(URL.EDIT)
    public String editMovie(@RequestParam int id, Model model) {
        MovieDto movie = movieMapper.toDto(movieService.findById(id));
        model.addAttribute(ModelAttr.OBJ, movie);
        Iterable<PersonDto> persons = personService.findAll().stream().map(personMapper::toDto).collect(Collectors.toList());
        Set<PersonDto> moviePersons = movie.getPersons();
        model.addAttribute(ModelAttr.PERSONS, persons );
        var languages = languageRepository.findAll().stream().map(languageMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.LANGUAGES, languages);
        model.addAttribute(ModelAttr.SELECTED_PERSONS, moviePersons);
        return Template.EDIT;
    }

    @PostMapping(URL.EDIT)
    public String updateMovie(MovieDto movie, int[] personIds) {
        movieService.update(movieMapper.toEntity(movie));
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region create movie
    @GetMapping(URL.CREATE)
    public String createMovie(Model model) {
        MovieDto movie = new MovieDto();
        model.addAttribute(ModelAttr.OBJ, movie);
        Iterable<PersonDto> persons = personService.findPaginated(1,20).stream().map(personMapper::toDto).collect(Collectors.toList());
        Set<PersonDto> moviePersons = movie.getPersons();
        var languages = languageRepository.findAll().stream().map(languageMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.LANGUAGES, languages);
        model.addAttribute(ModelAttr.PERSONS,persons );
        model.addAttribute(ModelAttr.SELECTED_PERSONS,moviePersons);
        return Template.CREATE;
    }

    @PostMapping(URL.CREATE)
    public String createMovie(@Valid MovieDto movie, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(ModelAttr.ERRORS, result.getAllErrors());
            return Template.CREATE;
        }
        movieService.update(movieMapper.toEntity(movie));
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region delete movie
    @GetMapping(URL.DELETE)
    public String deleteMovie(@RequestParam int id,
                              Model model,@SessionAttribute Optional<Integer> totalItems ) {
        if(totalItems.isPresent())
        {
            model.addAttribute(ModelAttr.TOTAL_ITEMS, totalItems.get() - 1);
        }
        movieService.deleteById(id);
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region select movies
    @GetMapping(URL.SELECT_ALL)
    public String selectAll(Model model, @SessionAttribute(ModelAttr.CURRENT_PAGE) Optional<Integer> pageNo,
                            @SessionAttribute(ModelAttr.PAGE_SIZE) Optional<Integer> pageSize,
                            @SessionAttribute(ModelAttr.SEARCH) Optional<String> search) {
        fillModelForGrid(pageNo, pageSize, search, model);
        return Template.SELECT_ALL;
    }

    @GetMapping (URL.REFRESH_LIST_DATA)
    public ModelAndView refreshDataForGrid(@RequestParam(Param.PAGE_NO) Optional<Integer> pageNo,
                                           @RequestParam(Param.PAGE_SIZE) Optional<Integer> pageSize,
                                           @RequestParam(Param.SEARCH) Optional<String> search,
                                           Model model) {
        fillModelForGrid(pageNo, pageSize, search, model);
        return new ModelAndView(Template.SELECT_ALL_DATA, model.asMap());

    }

    private void fillModelForGrid(Optional<Integer> pageNo, Optional<Integer> pageSize, Optional<String> search, Model model) {
        if(pageNo.isEmpty())
        {
            pageNo = Optional.of(Default.PAGE_NO);
        }
        if(pageSize.isEmpty())
        {
            pageSize = Optional.of(Default.PAGE_SIZE);
        }
        Page<MovieDto> page;
        if(search.isEmpty())
        {
            page = movieService
                    .findPaginated(pageNo.get(), pageSize.get()).map((Movie movie) -> movieMapper.toDto(movie));
        }
        else {
            page = movieService
                    .findPaginatedWithFilterByNameIgnoreCase
                            (pageNo.get(), pageSize.get(), search.get()).map((Movie movie) -> movieMapper.toDto(movie));
        }
        long totalItems = page.getTotalElements();
        int totalPagesCount = page.getTotalPages();
        model.addAttribute(ModelAttr.TOTAL_PAGES, totalPagesCount);
        model.addAttribute(ModelAttr.CURRENT_PAGE,
                pageNo.get() > totalPagesCount ? totalPagesCount : pageNo.get());
        model.addAttribute(ModelAttr.TOTAL_ITEMS, totalItems);
        model.addAttribute(ModelAttr.PAGE_SIZE, pageSize.get());
        model.addAttribute(ModelAttr.LIST, page.getContent());
        model.addAttribute(ModelAttr.SEARCH, search.orElse(null));
    }
    //endregion


    //region constants
    static final class URL {
        static final String SELECT_ALL = "movies";
        static final String REFRESH_LIST_DATA = "refresh_movies";
        static final String EDIT = "edit_movie";
        static final String CREATE = "create_movie";
        static final String DELETE = "delete_movie";
        static final String REDIRECT_TO_LIST = "redirect:/movies";
    }

    static final class Template {
        static final String SELECT_ALL = "movie/movie_list";
        static final String SELECT_ALL_DATA = "movie/movie_list_data";
        static final String EDIT = "movie/movie_edit_form";
        static final String CREATE = "movie/movie_add_form";
    }

    static final class ModelAttr {
        static final String ERRORS = "ERRORS";
        static final String CURRENT_PAGE = "movie_currentPage";
        static final String TOTAL_PAGES = "movie_totalPages";
        static final String TOTAL_ITEMS = "movie_totalItems";
        static final String PAGE_SIZE = "movie_pageSize";
        static final String SEARCH = "movie_search";
        static final String LIST = "movie_list";
        static final String COUNTRIES = "countries";
        static final String PERSONS = "persons";
        static final String SELECTED_PERSONS = "selectedPersons";
        static final String GENDERS = "genders";
        static final String DEPARTMENTS = "departments";
        static final String OBJ = "movieDto";
    }

    static final class Default {
        static final int PAGE_NO = 1;
        static final int PAGE_SIZE = 10;
    }

    static final class Param {
        static final String PAGE_NO = "pageNo";
        static final String PAGE_SIZE = "pageSize";
        static final String SEARCH = "search";
    }
    //endregion
}