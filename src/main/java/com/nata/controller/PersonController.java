package com.nata.controller;

import com.nata.dto.PersonDto;
import com.nata.dto.PersonGridDto;
import com.nata.mappers.CountryMapper;
import com.nata.mappers.DepartmentMapper;
import com.nata.mappers.GenderMapper;
import com.nata.mappers.PersonMapper;
import com.nata.model.Country;
import com.nata.model.Department;
import com.nata.model.Gender;
import com.nata.model.Person;
import com.nata.services.*;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;

import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@SessionAttributes({PersonController.ModelAttr.CURRENT_PAGE, PersonController.ModelAttr.TOTAL_PAGES, PersonController.ModelAttr.TOTAL_ITEMS, PersonController.ModelAttr.PAGE_SIZE})
public class PersonController {
    @org.springframework.web.bind.annotation.ModelAttribute(PersonController.ModelAttr.CURRENT_PAGE)
    public Integer currentPage() {
        return Default.PAGE_NO;
    }
    @org.springframework.web.bind.annotation.ModelAttribute(PersonController.ModelAttr.TOTAL_PAGES)
    public Integer totalPages() {
        return 1;
    }
    @org.springframework.web.bind.annotation.ModelAttribute(PersonController.ModelAttr.TOTAL_ITEMS)
    public Integer totalItems() {
        return 1;
    }
    @org.springframework.web.bind.annotation.ModelAttribute(PersonController.ModelAttr.PAGE_SIZE)
    public Integer pageSize() {
        return Default.PAGE_SIZE;
    }

    private final PersonMapper personMapper;
    private final CountryMapper countryMapper;
    private final DepartmentMapper departmentMapper;
    private final GenderMapper genderMapper;
    private final
    PersonService personService;
    private final
    CountryService countryService;
    private final
    DepartmentService departmentService;
    private final
    GenderService genderService;

    public PersonController(PersonMapper personMapper, CountryMapper countryMapper, DepartmentMapper departmentMapper,
                            GenderMapper genderMapper, PersonService personService, CountryService countryService,
                            DepartmentService departmentService, GenderService genderService) {
        this.personMapper = personMapper;
        this.countryMapper = countryMapper;
        this.departmentMapper = departmentMapper;
        this.genderMapper = genderMapper;
        this.personService = personService;
        this.countryService = countryService;
        this.departmentService = departmentService;
        this.genderService = genderService;
    }

    //region edit person
    @GetMapping(URL.EDIT)
    public String editPerson(@RequestParam int id, Model model) {
        PersonDto person = personMapper.toDto(personService.findById(id));
        model.addAttribute(PersonController.ModelAttr.OBJ, person);
        var genders = genderService.findAll().stream().map(genderMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.GENDERS, genders);
        var departments = departmentService.findAll().stream().map(departmentMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.DEPARTMENTS, departments);
        var countries = countryService.findAll().stream().map(countryMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.COUNTRIES, countries);
        return TemplatePath.EDIT_FORM;
    }

    @PostMapping(URL.EDIT)
    public String updatePerson(Person person, Optional<Integer> departmentId, Optional<Integer> countryId, Optional<Integer> genderId) {
        var gender = genderId.isPresent() ? genderService.findById(genderId.get()) : null;
        var department = departmentId.isPresent() ? departmentService.findById(departmentId.get()) : null;
        var country = countryId.isPresent() ? countryService.findById(countryId.get()) : null;
        if(department!= null)
            person.setDepartment(department);
        if(country!= null)
            person.setCountryCode(country);
        if(gender!= null)
            person.setGender(gender);
        personService.update(person);
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region create person
    @GetMapping(URL.CREATE)
    public String createPerson(Model model) {
        Person person = new Person();
        var genders = genderService.findAll().stream().map(genderMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.GENDERS, genders);
        var departments = departmentService.findAll().stream().map(departmentMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.DEPARTMENTS, departments);
        var countries = countryService.findAll().stream().map(countryMapper::toDto).collect(Collectors.toList());
        model.addAttribute(PersonController.ModelAttr.COUNTRIES, countries);
        model.addAttribute(PersonController.ModelAttr.OBJ, person);
        return TemplatePath.ADD_FORM;
    }
    @PostMapping(URL.CREATE)
    public String createPerson(@Valid Person person, BindingResult result, Model model) {
        //RequestParam("role_id") Integer roleId
        if (result.hasErrors()) {
            model.addAttribute(UserController.ModelAttr.ERRORS, result.getAllErrors());
            return URL.CREATE;
        }
        personService.update(person);

        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region delete person
    @GetMapping(URL.DELETE)
    public String deletePerson(@RequestParam int id,
                              Model model, @SessionAttribute Optional<Integer> totalItems) {
        if(totalItems.isPresent())
        {
            model.addAttribute(PersonController.ModelAttr.TOTAL_ITEMS, totalItems.get() - 1);
        }
        personService.deleteById(id);
        return URL.REDIRECT_TO_LIST;
    }
    //endregion

    //region select all persons
    @GetMapping(URL.SELECT_ALL)
    public String selectAll(Model model, @SessionAttribute(MovieController.ModelAttr.CURRENT_PAGE) Optional<Integer> pageNo,
                            @SessionAttribute(MovieController.ModelAttr.PAGE_SIZE) Optional<Integer> pageSize,
                            @SessionAttribute(MovieController.ModelAttr.SEARCH) Optional<String> search) {
        fillModelForGrid(pageNo, pageSize, search, model);
        return TemplatePath.LIST;
    }

    @GetMapping(URL.REFRESH_LIST_DATA)
    public ModelAndView selectAllData(@RequestParam(ParamName.PAGE_NO) Optional<Integer> pageNo,
                                      @RequestParam(ParamName.PAGE_SIZE) Optional<Integer> pageSize,
                                      @RequestParam(ParamName.SEARCH) Optional<String> search,
                                      Model model) {
        fillModelForGrid(pageNo, pageSize, search, model);
        return new ModelAndView(TemplatePath.LIST_DATA, model.asMap());
    }

    private void fillModelForGrid(Optional<Integer> pageNo, Optional<Integer> pageSize, Optional<String> search, Model model) {
        if(pageNo.isEmpty())
        {
            pageNo = Optional.of(Default.PAGE_NO);
        }
        if(pageSize.isEmpty())
        {
            pageSize = Optional.of(Default.PAGE_SIZE);
        }
        Page<PersonGridDto> page;
        if(search.isEmpty())
        {
            page = personService
                    .findPaginated(pageNo.get(), pageSize.get(), PersonGridDto.class);
        }
        else {
            page = personService
                    .findPaginatedWithFilterByFullNameIgnoreCase
                            (pageNo.get(), pageSize.get(), search.get(), PersonGridDto.class);
        }
        long totalItems = page.getTotalElements();
        int totalPagesCount = page.getTotalPages();
        model.addAttribute(ModelAttr.TOTAL_PAGES, totalPagesCount);
        model.addAttribute(ModelAttr.CURRENT_PAGE,
                pageNo.get() > totalPagesCount ? totalPagesCount : pageNo.get());
        model.addAttribute(ModelAttr.TOTAL_ITEMS, totalItems);
        model.addAttribute(ModelAttr.PAGE_SIZE, pageSize.get());
        model.addAttribute(ModelAttr.LIST, page.getContent());
        model.addAttribute(ModelAttr.SEARCH, search.orElse(null));
    }
    //endregion

    //region constants
    static final class URL {
        static final String SELECT_ALL = "persons";
        static final String REFRESH_LIST_DATA = "refresh_persons";
        static final String EDIT = "edit_person";
        static final String CREATE = "create_person";
        static final String DELETE = "delete_person";
        static final String REDIRECT_TO_LIST = "redirect:/persons";
    }

    static final class TemplatePath {
        static final String LIST = "person/person_list";
        static final String LIST_DATA = "person/person_list_data";
        static final String EDIT_FORM = "person/person_edit_form";
        static final String ADD_FORM = "person/person_add_form";
    }

    static final class ModelAttr {
        static final String ERRORS = "ERRORS";
        static final String CURRENT_PAGE = "person_currentPage";
        static final String TOTAL_PAGES = "person_totalPages";
        static final String TOTAL_ITEMS = "person_totalItems";
        static final String PAGE_SIZE = "person_pageSize";
        static final String LIST = "person_list";
        static final String SEARCH = "person_search";
        static final String COUNTRIES = "countries";
        static final String GENDERS = "genders";
        static final String LANGUAGES = "languages";
        static final String DEPARTMENTS = "departments";
        static final String OBJ = "obj";
    }

    static final class Default {
        static final int PAGE_NO = 1;
        static final int PAGE_SIZE = 10;
    }

    static final class ParamName {
        static final String PAGE_NO = "pageNo";
        static final String PAGE_SIZE = "pageSize";
        static final String SEARCH = "search";
    }
    //endregion
}