package com.nata.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditingController {
    Logger logger = LoggerFactory.getLogger(AuditingController.class);

    @Before("execution(* com.nata.controller.*Controller.*(..))")
    public void logBefore(JoinPoint joinPoint){
        log("BEFORE method ", joinPoint);
    }

    @AfterReturning("execution(* com.nata.controller.*Controller.*(..))")
    public void logAfter(JoinPoint joinPoint){
        log("AFTER method ", joinPoint);
    }


    @AfterThrowing("execution(* com.nata.controller.*Controller.*(..))")
    public void logError(JoinPoint joinPoint) {
        log("ERROR on method ", joinPoint);
    }

    private void log(String logPrefix, JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        String[] parametersName = ((MethodSignature) joinPoint.getSignature()).getParameterNames();
        StringBuilder argsBuilder = new StringBuilder();
        var executionInfo = joinPoint.toShortString();
        var jsonSerializer = new com.fasterxml.jackson.databind.ObjectMapper();
        if(parametersName.length > 0) {
            argsBuilder.append("\n\t").append(logPrefix)
                    .append(executionInfo).append("\n")
                    .append("\t\tParameters: ");

            for(var i= 0; i < parametersName.length; i++)
            {
                try {
                    argsBuilder.append("\n\t\t\t").append(parametersName[i])
                            .append(":\t\t\t").append(jsonSerializer.writeValueAsString(args[i]));
                } catch (JsonProcessingException e) {
                    logger.warn(logPrefix + " method execution: error on args serialization:" + joinPoint.toShortString());
                }
            }
        }
        else {
            argsBuilder.append(logPrefix)
                    .append(executionInfo);
        }

        logger.debug(argsBuilder.toString());
    }
}