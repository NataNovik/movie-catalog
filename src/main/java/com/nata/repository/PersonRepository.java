package com.nata.repository;
import com.nata.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Iterator;
import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Integer> {

    //List<Person> findByFullNameLike(String fullName);
    Page<Person> findByNameIgnoreCaseContaining(Pageable pageable, String name);
    List<Person> findByNameIgnoreCaseContainingOrId(String name, Integer id);
    <T> Page<T> findBy(Pageable pageable, Class<T> type);
    <T> Page<T> findByNameIgnoreCaseContaining(Pageable pageable, String name,  Class<T> type);

}