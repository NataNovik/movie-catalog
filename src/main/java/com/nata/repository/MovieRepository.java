package com.nata.repository;
import com.nata.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
  //  List<Movie> findByNameLike(String filmName);
    Page<Movie> findByTitleIgnoreCaseContaining(Pageable pageable, String title);
    List<Movie> findByTitleIgnoreCaseContainingOrId(String title, Integer id);
}