package com.nata.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Gender}
 */
public class GenderDto implements Serializable {
    @NotNull
    private Byte id;
    @NotNull
    @Size(max = 50)
    @NotEmpty
    @NotBlank
    private String genderName;

    public GenderDto() {
    }

    public GenderDto(@NotNull Byte id, @NotNull String genderName) {
        this.id = id;
        this.genderName = genderName;
    }

    public Byte getId() {
        return id;
    }

    public GenderDto setId(Byte id) {
        this.id = id;
        return this;
    }

    public String getGenderName() {
        return genderName;
    }

    public GenderDto setGenderName(String genderName) {
        this.genderName = genderName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenderDto entity = (GenderDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.genderName, entity.genderName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, genderName);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "genderName = " + genderName + ")";
    }
}