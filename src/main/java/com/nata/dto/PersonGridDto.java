package com.nata.dto;

import com.nata.model.Person;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * DTO for {@link Person}
 */
public interface PersonGridDto {

    public Integer getId();
    public String getName();
    public LocalDate getBirthday();
    public GenderDto getGender();
    public BigDecimal getPopularity();
    public String getImdbId();
}