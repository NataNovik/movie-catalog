package com.nata.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Language}
 */
public class LanguageDto implements Serializable {
    @NotNull
    @NotBlank
    @NotEmpty
    @Size(max = 10)
    private String code;
    @NotNull
    @NotBlank
    @NotEmpty
    @Size(max = 30)
    private String name;

    public LanguageDto() {
    }

    public LanguageDto(String code, @NotNull String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public LanguageDto setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }

    public LanguageDto setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LanguageDto entity = (LanguageDto) o;
        return Objects.equals(this.code, entity.code) &&
                Objects.equals(this.name, entity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "code = " + code + ", " +
                "name = " + name + ")";
    }
}