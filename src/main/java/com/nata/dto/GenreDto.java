package com.nata.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Genre}
 */
public class GenreDto implements Serializable {
    @NotNull
    private Short id;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 20)
    private String genre;

    public GenreDto() {
    }

    public GenreDto(Short id, @NotNull String genre) {
        this.id = id;
        this.genre = genre;
    }

    public Short getId() {
        return id;
    }

    public GenreDto setId(Short id) {
        this.id = id;
        return this;
    }

    public String getGenre() {
        return genre;
    }

    public GenreDto setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenreDto entity = (GenreDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.genre, entity.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, genre);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "genre = " + genre + ")";
    }
}