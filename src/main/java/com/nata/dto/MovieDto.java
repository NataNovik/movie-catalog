package com.nata.dto;
import jakarta.validation.constraints.*;
import com.nata.model.MovieReview;
import org.hibernate.validator.constraints.URL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * DTO for {@link com.nata.model.Movie}
 */
public class MovieDto implements Serializable {
    private Integer id;

    @NotEmpty
    @Size(max = 120)
    private String title;

    @Size(max = 40)
    private String backdropPath;

    @Positive
    private Integer budget;

    @URL
    private String homepage;

    @Size(max = 10)
    @Pattern(message = "Value shoud be in format like \"tt0123456\"", regexp = "tt[0-9]{7}|")
    private String imdbId;

    private LanguageDto originalLanguageCode;


    @Size(max = 120)
    private String originalTitle;


    private String overview;

    @Positive
    private BigDecimal popularity;

    @Size(max = 40)
    private String posterPath;

    private LocalDate releaseDate;

    private Long revenue;

    @Size(max = 10)
    private String runtime;

    @Positive
    private BigDecimal voteAverage;

    private Short voteCount;


    @Size(max = 20)
    private String status;

    private String tagline;

    private Integer collectionId;


    @URL
    @Size(max = 500)
    private String trailerLink;

    private Set<String> crewCreditCreditIds;

    private Set<Integer> kudagomovieIds;

    private Set<Integer> companyIds;

    private Set<GenreDto> genres;

    private Set<String> movieKeywords;
//
//
//    private Set<Integer> moviesInRecommendetionIds;
//
//    private Set<MovieDto> includedInRecommendations;

    private Set<MovieReview> movieReviews;

//    private Set<MovieDto> moviesInSimilars;
//
//    private Set<MovieDto> includedInSimilars;

    private Set<LanguageDto> spokenLanguages;


    public MovieDto(CollectionDto collection) {
    }

    public MovieDto()
    {

    }

    public MovieDto(Integer id, String title, String backdropPath, Integer budget, String homepage, String imdbId, LanguageDto originalLanguageCode, String originalTitle, String overview, BigDecimal popularity, String posterPath, LocalDate releaseDate, Long revenue, String runtime, BigDecimal voteAverage, Short voteCount, String status, String tagline
//            , CollectionDto collection
                    ,String trailerLink, Set<CrewCreditDto> crewCredits, Set<CompanyDto> companies, Set<GenreDto> genres, Set<String> movieKeywords, Set<MovieDto> moviesInRecommendetions, Set<MovieDto> includedInRecommendations, Set<MovieReview> movieReviews, Set<MovieDto> moviesInSimilars, Set<MovieDto> includedInSimilars, Set<LanguageDto> spokenLanguages) {
        this.id = id;
        this.title = title;
        this.backdropPath = backdropPath;
        this.budget = budget;
        this.homepage = homepage;
        this.imdbId = imdbId;
        this.originalLanguageCode = originalLanguageCode;
        this.originalTitle = originalTitle;
        this.overview = overview;
        this.popularity = popularity;
        this.posterPath = posterPath;
        this.releaseDate = releaseDate;
        this.revenue = revenue;
        this.runtime = runtime;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.status = status;
        this.tagline = tagline;
        this.trailerLink = trailerLink;
        this.genres = genres;
        this.movieKeywords = movieKeywords;
//        this.includedInRecommendations = includedInRecommendations;
        this.movieReviews = movieReviews;
//        this.moviesInSimilars = moviesInSimilars;
//        this.includedInSimilars = includedInSimilars;
        this.spokenLanguages = spokenLanguages;
    }

    public Integer getId() {
        return id;
    }

    public MovieDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public MovieDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public MovieDto setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
        return this;
    }

    public Integer getBudget() {
        return budget;
    }

    public MovieDto setBudget(Integer budget) {
        this.budget = budget;
        return this;
    }

    public String getHomepage() {
        return homepage;
    }

    public MovieDto setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public String getImdbId() {
        return imdbId;
    }

    public MovieDto setImdbId(String imdbId) {
        this.imdbId = imdbId;
        return this;
    }

    public LanguageDto getOriginalLanguageCode() {
        return originalLanguageCode;
    }

    public MovieDto setOriginalLanguageCode(LanguageDto originalLanguageCode) {
        this.originalLanguageCode = originalLanguageCode;
        return this;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public MovieDto setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
        return this;
    }

    public String getOverview() {
        return overview;
    }

    public MovieDto setOverview(String overview) {
        this.overview = overview;
        return this;
    }

    public BigDecimal getPopularity() {
        return popularity;
    }

    public MovieDto setPopularity(BigDecimal popularity) {
        this.popularity = popularity;
        return this;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public MovieDto setPosterPath(String posterPath) {
        this.posterPath = posterPath;
        return this;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public MovieDto setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public Long getRevenue() {
        return revenue;
    }

    public MovieDto setRevenue(Long revenue) {
        this.revenue = revenue;
        return this;
    }

    public String getRuntime() {
        return runtime;
    }

    public MovieDto setRuntime(String runtime) {
        this.runtime = runtime;
        return this;
    }

    public BigDecimal getVoteAverage() {
        return voteAverage;
    }

    public MovieDto setVoteAverage(BigDecimal voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public Short getVoteCount() {
        return voteCount;
    }

    public MovieDto setVoteCount(Short voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public MovieDto setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getTagline() {
        return tagline;
    }

    public MovieDto setTagline(String tagline) {
        this.tagline = tagline;
        return this;
    }


//    public MovieDto setCollection(CollectionDto collection) {
//        return this;
//    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public MovieDto setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
        return this;
    }

    public Set<GenreDto> getGenres() {
        return genres;
    }

    public MovieDto setGenres(Set<GenreDto> genres) {
        this.genres = genres;
        return this;
    }

    public Set<String> getMovieKeywords() {
        return movieKeywords;
    }

    public MovieDto setMovieKeywords(Set<String> movieKeywords) {
        this.movieKeywords = movieKeywords;
        return this;
    }
/*
    public Set<MovieDto> getIncludedInRecommendations() {
        return includedInRecommendations;
    }

    public MovieDto setIncludedInRecommendations(Set<MovieDto> includedInRecommendations) {
        this.includedInRecommendations = includedInRecommendations;
        return this;
    }
*/
    public Set<MovieReview> getMovieReviews() {
        return movieReviews;
    }

    public MovieDto setMovieReviews(Set<MovieReview> movieReviews) {
        this.movieReviews = movieReviews;
        return this;
    }
/*
    public Set<MovieDto> getMoviesInSimilars() {
        return moviesInSimilars;
    }

    public MovieDto setMoviesInSimilars(Set<MovieDto> moviesInSimilars) {
        this.moviesInSimilars = moviesInSimilars;
        return this;
    }

    public Set<MovieDto> getIncludedInSimilars() {
        return includedInSimilars;
    }

    public MovieDto setIncludedInSimilars(Set<MovieDto> includedInSimilars) {
        this.includedInSimilars = includedInSimilars;
        return this;
    }
*/
    public Set<LanguageDto> getSpokenLanguages() {
        return spokenLanguages;
    }

    public MovieDto setSpokenLanguages(Set<LanguageDto> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
        return this;
    }

//    public Integer getCollectionId() {
//        return collectionId;
//    }
//
//    public void setCollectionId(Integer collectionId) {
//        this.collectionId = collectionId;
//    }

    public Set<String> getCrewCreditCreditIds() {
        return crewCreditCreditIds;
    }

    public void setCrewCreditCreditIds(Set<String> crewCreditCreditIds) {
        this.crewCreditCreditIds = crewCreditCreditIds;
    }

    public Set<Integer> getKudagomovieIds() {
        return kudagomovieIds;
    }

    public void setKudagomovieIds(Set<Integer> kudagomovieIds) {
        this.kudagomovieIds = kudagomovieIds;
    }

    public Set<Integer> getCompanyIds() {
        return companyIds;
    }

    public void setCompanyIds(Set<Integer> companyIds) {
        this.companyIds = companyIds;
    }
/*
    public Set<Integer> getMoviesInRecommendetionIds() {
        return moviesInRecommendetionIds;
    }

    public void setMoviesInRecommendetionIds(Set<Integer> moviesInRecommendetionIds) {
        this.moviesInRecommendetionIds = moviesInRecommendetionIds;
    }
*/
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "MovieDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", backdropPath='" + backdropPath + '\'' +
                ", budget=" + budget +
                ", homepage='" + homepage + '\'' +
                ", imdbId='" + imdbId + '\'' +
                ", originalLanguageCode=" + originalLanguageCode +
                ", originalTitle='" + originalTitle + '\'' +
                ", overview='" + overview + '\'' +
                ", popularity=" + popularity +
                ", posterPath='" + posterPath + '\'' +
                ", releaseDate=" + releaseDate +
                ", revenue=" + revenue +
                ", runtime='" + runtime + '\'' +
                ", voteAverage=" + voteAverage +
                ", voteCount=" + voteCount +
                ", status='" + status + '\'' +
                ", tagline='" + tagline + '\'' +
//                ", collectionId=" + collectionId +
                ", trailerLink='" + trailerLink + '\'' +
                ", crewCreditCreditIds=" + crewCreditCreditIds +
                ", kudagomovieIds=" + kudagomovieIds +
                ", companyIds=" + companyIds +
                ", genres=" + genres +
                ", movieKeywords=" + movieKeywords +
//                ", moviesInRecommendetionIds=" + moviesInRecommendetionIds +
//                ", includedInRecommendations=" + includedInRecommendations +
                ", movieReviews=" + movieReviews +
//                ", moviesInSimilars=" + moviesInSimilars +
//                ", includedInSimilars=" + includedInSimilars +
                ", spokenLanguages=" + spokenLanguages +
                '}';
    }

    public Set<PersonDto> getPersons() {
        return Collections.emptySet();
    }
}