package com.nata.dto;

import com.nata.model.Person;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * DTO for {@link Person}
 */
public class PersonDto implements Serializable {
    @NotNull
    private Integer id;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 50)
    private String name;
    @NotNull
    private DepartmentDto department;
    @NotNull
    private LocalDate birthday;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 1000)
    private String alsoKnownAs;
    @NotNull
    private LocalDate deathday;
    @NotNull
    private GenderDto gender;
    @NotNull
    @NotEmpty
    @NotBlank
    private String biography;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 40)
    private String profilePath;
    @NotNull
    private BigDecimal popularity;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 100)
    private String placeOfBirth;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 10)
    private String imdbId;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 160)
    private String homepage;
    @NotNull
    private CountryDto countryCode;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 200)
    private String additionalInfo;

    public PersonDto() {
    }

    public PersonDto(Integer id, @NotNull String name, @NotNull DepartmentDto department, LocalDate birthday, String alsoKnownAs, LocalDate deathday,
                     GenderDto gender, String biography, String profilePath, @NotNull BigDecimal popularity, String placeOfBirth, String imdbId, String homepage, CountryDto countryCode, String additionalInfo) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.birthday = birthday;
        this.alsoKnownAs = alsoKnownAs;
        this.deathday = deathday;
        this.gender = gender;
        this.biography = biography;
        this.profilePath = profilePath;
        this.popularity = popularity;
        this.placeOfBirth = placeOfBirth;
        this.imdbId = imdbId;
        this.homepage = homepage;
        this.countryCode = countryCode;
        this.additionalInfo = additionalInfo;
    }

    public Integer getId() {
        return id;
    }

    public PersonDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public PersonDto setName(String name) {
        this.name = name;
        return this;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public PersonDto setDepartment(DepartmentDto department) {
        this.department = department;
        return this;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public PersonDto setBirthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getAlsoKnownAs() {
        return alsoKnownAs;
    }

    public PersonDto setAlsoKnownAs(String alsoKnownAs) {
        this.alsoKnownAs = alsoKnownAs;
        return this;
    }

    public LocalDate getDeathday() {
        return deathday;
    }

    public PersonDto setDeathday(LocalDate deathday) {
        this.deathday = deathday;
        return this;
    }

    public
    GenderDto getGender() {
        return gender;
    }

    public PersonDto setGender(GenderDto gender) {
        this.gender = gender;
        return this;
    }

    public String getBiography() {
        return biography;
    }

    public PersonDto setBiography(String biography) {
        this.biography = biography;
        return this;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public PersonDto setProfilePath(String profilePath) {
        this.profilePath = profilePath;
        return this;
    }

    public BigDecimal getPopularity() {
        return popularity;
    }

    public PersonDto setPopularity(BigDecimal popularity) {
        this.popularity = popularity;
        return this;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public PersonDto setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public String getImdbId() {
        return imdbId;
    }

    public PersonDto setImdbId(String imdbId) {
        this.imdbId = imdbId;
        return this;
    }

    public String getHomepage() {
        return homepage;
    }

    public PersonDto setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public CountryDto getCountryCode() {
        return countryCode;
    }

    public PersonDto setCountryCode(CountryDto countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public PersonDto setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDto entity = (PersonDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.name, entity.name) &&
                Objects.equals(this.department, entity.department) &&
                Objects.equals(this.birthday, entity.birthday) &&
                Objects.equals(this.alsoKnownAs, entity.alsoKnownAs) &&
                Objects.equals(this.deathday, entity.deathday) &&
                Objects.equals(this.gender, entity.gender) &&
                Objects.equals(this.biography, entity.biography) &&
                Objects.equals(this.profilePath, entity.profilePath) &&
                Objects.equals(this.popularity, entity.popularity) &&
                Objects.equals(this.placeOfBirth, entity.placeOfBirth) &&
                Objects.equals(this.imdbId, entity.imdbId) &&
                Objects.equals(this.homepage, entity.homepage) &&
                Objects.equals(this.countryCode, entity.countryCode) &&
                Objects.equals(this.additionalInfo, entity.additionalInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, department, birthday, alsoKnownAs, deathday, gender, biography, profilePath, popularity, placeOfBirth, imdbId, homepage, countryCode, additionalInfo);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ", " +
                "department = " + department + ", " +
                "birthday = " + birthday + ", " +
                "alsoKnownAs = " + alsoKnownAs + ", " +
                "deathday = " + deathday + ", " +
                "gender = " + gender + ", " +
                "biography = " + biography + ", " +
                "profilePath = " + profilePath + ", " +
                "popularity = " + popularity + ", " +
                "placeOfBirth = " + placeOfBirth + ", " +
                "imdbId = " + imdbId + ", " +
                "homepage = " + homepage + ", " +
                "countryCode = " + countryCode + ", " +
                "additionalInfo = " + additionalInfo + ")";
    }
}