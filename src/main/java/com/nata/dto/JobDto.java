package com.nata.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Job}
 */
public class JobDto implements Serializable {
    @NotNull
    private Short id;
    @NotNull
    @NotBlank
    @NotEmpty
    @Size(max = 50)
    private String job;

    public JobDto() {
    }

    public JobDto(Short id, @NotNull String job) {
        this.id = id;
        this.job = job;
    }

    public Short getId() {
        return id;
    }

    public JobDto setId(Short id) {
        this.id = id;
        return this;
    }

    public String getJob() {
        return job;
    }

    public JobDto setJob(String job) {
        this.job = job;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobDto entity = (JobDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.job, entity.job);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, job);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "job = " + job + ")";
    }
}