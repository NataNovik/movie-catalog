package com.nata.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.hibernate.validator.constraints.URL;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * DTO for {@link com.nata.model.Company}
 */
public class CompanyDto implements Serializable {
    private Integer id;
    @Size(max = 180)
    @URL
    private String homepage;
    @Size(max = 2000)
    private String description;
    @Size(max = 90)
    private String headquerters;
    @Size(max = 40)
    private String logoPath;
    @NotNull
    @Size(max = 110)
    private String name;
    @Size(max = 2)
    private String originCountryCode;
    private Integer parentCompanyId;
    private String parentCompanyName;
    private Set<MovieDto> movies;

    public CompanyDto() {
    }

    public CompanyDto(Integer id, String homepage, String description, String headquerters, String logoPath, @NotNull String name, String originCountryCode, Integer parentCompanyId, String parentCompanyName, Set<MovieDto> movies) {
        this.id = id;
        this.homepage = homepage;
        this.description = description;
        this.headquerters = headquerters;
        this.logoPath = logoPath;
        this.name = name;
        this.originCountryCode = originCountryCode;
        this.parentCompanyId = parentCompanyId;
        this.parentCompanyName = parentCompanyName;
        this.movies = movies;
    }

    public Integer getId() {
        return id;
    }

    public CompanyDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getHomepage() {
        return homepage;
    }

    public CompanyDto setHomepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CompanyDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getHeadquerters() {
        return headquerters;
    }

    public CompanyDto setHeadquerters(String headquerters) {
        this.headquerters = headquerters;
        return this;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public CompanyDto setLogoPath(String logoPath) {
        this.logoPath = logoPath;
        return this;
    }

    public String getName() {
        return name;
    }

    public CompanyDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public CompanyDto setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
        return this;
    }

    public Integer getParentCompanyId() {
        return parentCompanyId;
    }

    public CompanyDto setParentCompanyId(Integer parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
        return this;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public CompanyDto setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
        return this;
    }

    public Set<MovieDto> getMovies() {
        return movies;
    }

    public CompanyDto setMovies(Set<MovieDto> movies) {
        this.movies = movies;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyDto entity = (CompanyDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.homepage, entity.homepage) &&
                Objects.equals(this.description, entity.description) &&
                Objects.equals(this.headquerters, entity.headquerters) &&
                Objects.equals(this.logoPath, entity.logoPath) &&
                Objects.equals(this.name, entity.name) &&
                Objects.equals(this.originCountryCode, entity.originCountryCode) &&
                Objects.equals(this.parentCompanyId, entity.parentCompanyId) &&
                Objects.equals(this.parentCompanyName, entity.parentCompanyName) &&
                Objects.equals(this.movies, entity.movies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, homepage, description, headquerters, logoPath, name, originCountryCode, parentCompanyId, parentCompanyName, movies);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "homepage = " + homepage + ", " +
                "description = " + description + ", " +
                "headquerters = " + headquerters + ", " +
                "logoPath = " + logoPath + ", " +
                "name = " + name + ", " +
                "originCountryCode = " + originCountryCode + ", " +
                "parentCompanyId = " + parentCompanyId + ", " +
                "parentCompanyName = " + parentCompanyName + ", " +
                "movies = " + movies + ")";
    }
}