package com.nata.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * DTO for {@link com.nata.model.Collection}
 */
public class CollectionDto implements Serializable {
    private Integer id;
    @NotNull
    @Size(max = 80)
    private String name;
    private Set<MovieDto> movies;

    public CollectionDto() {
    }

    public CollectionDto(Integer id, @NotNull String name, Set<MovieDto> movies) {
        this.id = id;
        this.name = name;
        this.movies = movies;
    }

    public Integer getId() {
        return id;
    }

    public CollectionDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CollectionDto setName(String name) {
        this.name = name;
        return this;
    }

    public Set<MovieDto> getMovies() {
        return movies;
    }

    public CollectionDto setMovies(Set<MovieDto> movies) {
        this.movies = movies;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CollectionDto entity = (CollectionDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.name, entity.name) &&
                Objects.equals(this.movies, entity.movies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, movies);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ", " +
                "movies = " + movies + ")";
    }
}