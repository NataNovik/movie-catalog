package com.nata.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import com.nata.model.CrewCredit;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link CrewCredit}
 */
public class CrewCreditDto implements Serializable {
    @Size(max = 30)
    private String creditId;
    @NotNull
    private MovieDto film;
    private DepartmentDto department;
    private PersonDto person;
    private JobDto job;

    public CrewCreditDto() {
    }

    public CrewCreditDto(String creditId, @NotNull MovieDto film, DepartmentDto department, PersonDto person, JobDto job) {
        this.creditId = creditId;
        this.film = film;
        this.department = department;
        this.person = person;
        this.job = job;
    }

    public String getCreditId() {
        return creditId;
    }

    public CrewCreditDto setCreditId(String creditId) {
        this.creditId = creditId;
        return this;
    }

    public MovieDto getFilm() {
        return film;
    }

    public CrewCreditDto setFilm(MovieDto film) {
        this.film = film;
        return this;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public CrewCreditDto setDepartment(DepartmentDto department) {
        this.department = department;
        return this;
    }

    public PersonDto getPerson() {
        return person;
    }

    public CrewCreditDto setPerson(PersonDto person) {
        this.person = person;
        return this;
    }

    public JobDto getJob() {
        return job;
    }

    public CrewCreditDto setJob(JobDto job) {
        this.job = job;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CrewCreditDto entity = (CrewCreditDto) o;
        return Objects.equals(this.creditId, entity.creditId) &&
                Objects.equals(this.film, entity.film) &&
                Objects.equals(this.department, entity.department) &&
                Objects.equals(this.person, entity.person) &&
                Objects.equals(this.job, entity.job);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creditId, film, department, person, job);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "creditId = " + creditId + ", " +
                "film = " + film + ", " +
                "department = " + department + ", " +
                "person = " + person + ", " +
                "job = " + job + ")";
    }
}