package com.nata.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Country}
 */
public class CountryDto implements Serializable {
    @Size(max = 2)
    private String code;
    @NotNull
    @Size(max = 50)
    private String country;

    public CountryDto() {
    }

    public CountryDto(String code, @NotNull String country) {
        this.code = code;
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public CountryDto setCode(String code) {
        this.code = code;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public CountryDto setCountry(String country) {
        this.country = country;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryDto entity = (CountryDto) o;
        return Objects.equals(this.code, entity.code) &&
                Objects.equals(this.country, entity.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, country);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "code = " + code + ", " +
                "country = " + country + ")";
    }
}