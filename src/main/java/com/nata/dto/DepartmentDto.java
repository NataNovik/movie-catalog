package com.nata.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link com.nata.model.Department}
 */
public class DepartmentDto implements Serializable {
    private Byte id;
    @NotNull
    @Size(max = 20)
    private String department;

    public DepartmentDto() {
    }

    public DepartmentDto(Byte id, @NotNull String department) {
        this.id = id;
        this.department = department;
    }

    public Byte getId() {
        return id;
    }

    public DepartmentDto setId(Byte id) {
        this.id = id;
        return this;
    }

    public String getDepartment() {
        return department;
    }

    public DepartmentDto setDepartment(String department) {
        this.department = department;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentDto entity = (DepartmentDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.department, entity.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, department);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "department = " + department + ")";
    }
}