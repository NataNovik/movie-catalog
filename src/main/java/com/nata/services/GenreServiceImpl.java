package com.nata.services;

import com.nata.model.Genre;
import com.nata.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    GenreRepository GenreRepository;

    @Override
    public Collection<Genre> findAll() {
        return GenreRepository.findAll();
    }

    @Override
    public Genre findById(int id) {
        return GenreRepository.findById(id).orElse(null);
    }

    @Override
    public void insert(Genre Genre) {
        GenreRepository.save(Genre);
    }

    @Override
    public void update(Genre Genre) {
        GenreRepository.save(Genre);
    }

    @Override
    public void deleteById(int id) {
        GenreRepository.deleteById(id);
    }
}