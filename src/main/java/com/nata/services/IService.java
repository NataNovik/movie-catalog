package com.nata.services;

import java.util.Collection;
import java.util.List;

interface IService<T> {

    public Collection<T> findAll() ;

    public T findById(int id) ;

    public void insert(T entity);

    public void update(T entity);

    public void deleteById(int id);
}