package com.nata.services;

import com.nata.repository.UserRepository;


import com.nata.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(int id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void insert(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
    }

    @Override
    public void update(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
    }

    @Override
    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getUserByUserName(String userName)
    {
        return userRepository.getUserByUserName(userName);
    }

    @Override
    public Page<User> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.userRepository.findAll(pageable);
    }
    @Override
    public Page<User> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize, String fullNamePart) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.userRepository.findByFullNameIgnoreCaseContaining(pageable, fullNamePart);
    }
}