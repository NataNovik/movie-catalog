package com.nata.services;

import com.nata.model.Gender;
import com.nata.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class GenderServiceImpl implements GenderService {

    @Autowired
    GenderRepository genderRepository;

    @Override
    public Collection<Gender> findAll() {
        return genderRepository.findAll();
    }

    @Override
    public Gender findById(int id) {
        return genderRepository.findById(id).orElse(null);
    }

    @Override
    public void insert(Gender gender) {
        genderRepository.save(gender);
    }

    @Override
    public void update(Gender gender) {
        genderRepository.save(gender);
    }

    @Override
    public void deleteById(int id) {
        genderRepository.deleteById(id);
    }
}