package com.nata.services;
import com.nata.model.Movie;
import org.springframework.data.domain.Page;

public interface MovieService extends IService<Movie> {
//    void update(Movie movie, int[] personIds);
    Page<Movie> findPaginated(int pageNo, int pageSize);
    Page<Movie> findPaginatedWithFilterByNameIgnoreCase(int pageNo, int pageSize, String fullName);

}