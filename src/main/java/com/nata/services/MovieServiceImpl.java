package com.nata.services;

import com.nata.model.Movie;
import com.nata.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    MovieRepository movieRepository;
    @Autowired
    PersonService personService;

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie findById(int id) {
        return movieRepository.findById(id).orElse(null);
    }

    @Override
    public void insert(Movie entity) {
        movieRepository.save(entity);
    }

    @Override
    public void update(Movie movie) {
        movieRepository.save(movie);
    }
/*
    @Override
    public void update(Movie movie, int[] personIds) {
        var newPersons = new HashSet<Person>();
        ;
        for (var person: personService.findAll()) {
            for (var newPersonId: personIds) {
                if(newPersonId == person.getId())
                {
                    newPersons.add(person);
                    break;
                }
            }
        };
        for (CrewCredit crewCredit : movie.getCrewCredits()) {
            for (var newPersonId: personIds) {
                if(newPersonId == crewCredit.getPerson().getId())
                {
                    newPersons.(person);
                    break;
                }
            }
        }
        movie.set(newPersons);
        movieRepository.save(movie);
    }*/

    @Override
    public Page<Movie> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.movieRepository.findAll(pageable);
    }

    @Override
    public Page<Movie> findPaginatedWithFilterByNameIgnoreCase(int pageNo, int pageSize,
                                                               String namePart) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.movieRepository.findByTitleIgnoreCaseContaining(pageable, namePart);
    }


    @Override
    public void deleteById(int id) {
        movieRepository.deleteById(id);
    }
}