package com.nata.services;

import com.nata.dto.PersonGridDto;
import com.nata.model.Person;
import org.springframework.data.domain.Page;


public interface PersonService extends IService<Person>
{
    Page<Person> findPaginated(int pageNo, int pageSize);
    Page<Person> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize, String fullName);
    <T> Page<T> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize, String fullName,  Class<T> type);
    <T> Page<T> findPaginated(int pageNo, int pageSize, Class<T> type);
}