package com.nata.services;

import com.nata.model.User;
import org.springframework.data.domain.Page;

public interface UserService extends IService<User>
{
    public User getUserByUserName(String userName);

    Page<User> findPaginated(int pageNo, int pageSize);
    Page<User> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize, String fullName);
}