package com.nata.services;

import com.nata.model.Person;
import com.nata.repository.PersonRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository;

    @Override
    public Collection<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public Person findById(int id) {
        return personRepository.findById(id).orElse(null);
    }

    @Override
    public void insert(Person entity) {
        personRepository.save(entity);
    }

    @Override
    public void update(Person person) {
        personRepository.save(person);
    }

    @Override
    public void deleteById(int id) {
        personRepository.deleteById(id);
    }

    @Override
    public Page<Person> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.personRepository.findAll(pageable);
    }
    @Override
    public Page<Person> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize,
                                                                   String fullNamePart) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.personRepository.findByNameIgnoreCaseContaining(pageable, fullNamePart);
    }

    @Override
    public <T> Page<T> findPaginatedWithFilterByFullNameIgnoreCase(int pageNo, int pageSize, String fullName, Class<T> type) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.personRepository.findByNameIgnoreCaseContaining(pageable, fullName, type);
    }

    @Override
    public <T> Page<T> findPaginated(int pageNo, int pageSize, Class<T> type) {
        Pageable pageable = PageRequest.of(pageNo >= 2 ? pageNo - 1 : 1, pageSize >= 1 ? pageSize : 1);
        return this.personRepository.findBy(pageable, type);
    }
}